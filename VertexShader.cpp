#include "VertexShader.h"


VertexShader::VertexShader(ID3D11Device *_device, ID3DBlob *buffer, std::vector<D3D11_INPUT_ELEMENT_DESC>* shaderInterface)
{
	HRESULT result = _device->CreateVertexShader(buffer->GetBufferPointer(), buffer->GetBufferSize(), 0, &shader);

	if (FAILED(result))
	{
		return;
	}

	D3D11_INPUT_ELEMENT_DESC * test = new D3D11_INPUT_ELEMENT_DESC[shaderInterface->size()];

	for (int i = 0; i < shaderInterface->size(); i++)
	{
		test[i] = (*shaderInterface)[i];
	}

	unsigned int totalLayoutElements = shaderInterface->size();

	LPVOID bp = buffer->GetBufferPointer();
	SIZE_T bs = buffer->GetBufferSize();

	result = _device->CreateInputLayout(test, totalLayoutElements, buffer->GetBufferPointer(), buffer->GetBufferSize(), &inputLayout);

	buffer->Release();
}


VertexShader::~VertexShader()
{
}

ID3D11VertexShader* VertexShader::GetShader()
{
	return shader;
}

ID3D11InputLayout* VertexShader::GetInputLayout()
{
	return inputLayout;
}
