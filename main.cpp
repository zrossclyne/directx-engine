/*

Zachary Ross-Clyne (UP721941)
Computer Games Technology
CT5PRGAP - Program 3D Applications

*/

#pragma once

//#include <vld.h> //<- When you have a memory leak on PC 1.125D 7

#include <windows.h>

#include <memory>

#include <iostream>

#include "DXEngine.h"

#include "Camera.h"

#include "../DirectXTK-feb2016/Inc/SpriteBatch.h"
#include "../DirectXTK-feb2016/Inc/SpriteFont.h"
#include "../DirectXTK-feb2016/Inc/CommonStates.h"
#include "../DirectXTK-feb2016/Inc/DDSTextureLoader.h"


#include <string>

#include <sstream>


// Variables
DXEngine *engine;

enum Direction
{
	LEFT,
	RIGHT,
	FORWARD,
	BACKWARD
};

struct Pacman
{
	Direction curDirection = Direction::LEFT;

	Entity* top;
	Entity* bottom;

	bool animating = false;
	bool in = false;

	int lives = 3;

	void SetScale(float x, float y, float z)
	{
		top->SetScale({ x, y, z });
		bottom->SetScale({ x, y, z });
	}

	void SetPosition(float x, float y, float z)
	{
		top->SetPosition({ x, y, z });
		bottom->SetPosition({ x, y, z });
	}

	DirectX::XMFLOAT3 GetPosition()
	{
		DirectX::XMFLOAT3 topPos = top->GetPosition();
		DirectX::XMFLOAT3 bottomPos = bottom->GetPosition();

		DirectX::XMFLOAT3 rtn((topPos.x + bottomPos.x) / 2.0f, (topPos.y + bottomPos.y) / 2.0f, (topPos.z + bottomPos.z) / 2.0f);

		return rtn;
	}

	void Animate()
	{
		if (this->animating)
		{
			if (this->in)
			{
				this->top->SetRotation({ this->top->GetRotation().x, this->top->GetRotation().y, this->top->GetRotation().z - 0.01f });
				this->bottom->SetRotation({ this->bottom->GetRotation().x, this->bottom->GetRotation().y, this->bottom->GetRotation().z + 0.01f });

				if (this->top->GetRotation().z < -DirectX::XM_PI / 6.0f)
				{
					this->in = !this->in;
				}
			}
			else
			{
				this->top->SetRotation({ this->top->GetRotation().x, this->top->GetRotation().y, this->top->GetRotation().z + 0.01f });
				this->bottom->SetRotation({ this->bottom->GetRotation().x, this->bottom->GetRotation().y, this->bottom->GetRotation().z - 0.01f });

				if (this->top->GetRotation().z > 0.0f)
				{
					this->in = !this->in;
				}
			}
		}
	}
};

struct Pill
{
	Entity* entity;
	bool powerPill;

	bool visible = true;

	char* name;

	int x;
	int z;
};

// Prototypes
Global::Enums::GameState Run();
Global::Enums::GameState HandleInput();
void Update();
void InitDX();
void Exit();
void GamePadDown(int gamepadIndex, GamePadInput keyPressed, float value);
void GamePadUp(int gamepadIndex, GamePadInput keyPressed);

void ChangePacDirection(Pacman* p);
void MovePacman(Pacman* p);
Pill* GetPill(int x, int z);

GameState CheckPacPlayerCollision();

void LevelUp();
void ResetPills();
void CheckPills();
void RotatePac(Direction prevDir, Direction newDir);

void OnRender();

void Move(float x, float y);
void Rotate(float x, float y);
void ManageCameraPosition();
DirectX::XMFLOAT3 operator+(DirectX::XMFLOAT3 one, DirectX::XMFLOAT3 two);
DirectX::XMFLOAT3 operator-(DirectX::XMFLOAT3 one, DirectX::XMFLOAT3 two);
float Magnitude(DirectX::XMFLOAT3 vec);
void CameraCollision();

Camera* gameCamera;
Pacman pacman;
Entity* ghost;

int curScore = 0;

float SPEED_MULTI = 0.1f;
float ROT_SPEED_MULTI = 0.05f;

int level = 1;

float optimumCameraY;

std::vector<Entity*> mapObjects;
std::vector<Pill*> pacCollectables;

std::unique_ptr<DirectX::SpriteFont> spriteFont;

int map[21][19] = 
{
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 
	{ 1, 3, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 3, 1 }, 
	{ 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1 },
	{ 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1 },
	{ 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1 },
	{ 1, 2, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 2, 1 },
	{ 1, 2, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 1 },
	{ 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1 },
	{ 0, 0, 0, 1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 0, 0, 0 },
	{ 0, 0, 0, 1, 2, 1, 2, 1, 1, 0, 1, 1, 2, 1, 2, 1, 0, 0, 0 },
	{ 0, 0, 0, 1, 2, 2, 2, 1, 0, 0, 0, 1, 2, 2, 2, 1, 0, 0, 0 }, // middle line
	{ 0, 0, 0, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 0, 0, 0 },
	{ 0, 0, 0, 1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 1, 0, 0, 0 },
	{ 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1 },
	{ 1, 2, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 1 },
	{ 1, 2, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 1, 1, 2, 1 },
	{ 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1 },
	{ 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1 },
	{ 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 1, 1, 2, 1, 1, 2, 1 },
	{ 1, 3, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 3, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
};

int i = 0;

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{
	// Entry point (creates DirectXEngine instance and starts it
	engine = new DXEngine("ClassName", "WindowName", hInstance, prevInstance, cmdLine, cmdShow);

	return engine->Start(InitDX, Run, Exit);
}

void CheckPills()
{
	bool complete = true;
	for (int i = 0; i < pacCollectables.size(); i++)
	{
		pacCollectables[i]->entity->visible = pacCollectables[i]->visible;
		if (pacCollectables[i]->visible)
		{
			complete = false;
		}
	}

	if (complete)
	{
		LevelUp();
	}
}

void LevelUp()
{
	SPEED_MULTI += 0.05f;
	level++;

	ResetPills();
}

void ResetPills()
{
	for (int i = 0; i < pacCollectables.size(); i++)
	{
		pacCollectables[i]->visible = true;
	}
}

void InitDX()
{
	srand(GetTickCount());

	engine->EnableDirectInput(1);
	engine->EnableXInput(1);

	engine->GamePadPressed = GamePadDown;
	engine->GamePadUnpressed = GamePadUp;

	engine->SetCursorVisibility(false);
	engine->LockCursorPosition(true);

	engine->SetDeadZone(0.1f);

	MeshManager* meshManager = engine->GetDXManager()->GetMeshManager();
	TextureManager* texManager = engine->GetDXManager()->GetTextureManager();
	ShaderManager* shdManager = engine->GetDXManager()->GetShaderManager();
	EntityManager* entManager = engine->GetDXManager()->GetEntityManager();

	// Load Meshes
	//meshManager->AddMesh("custom", "sphere.obj");
	meshManager->AddMesh("cube", "cube.obj");

	meshManager->AddMesh("Ghost", "Ghost.obj");
	meshManager->AddMesh("PacManTop", "PacMan.obj");
	meshManager->AddMesh("PacManBottom", "PacMan-2.obj");

	meshManager->AddMesh("sphere", "sphere.obj");
	//meshManager->AddMesh("knot", "knot.obj");

	// Load Textures
	//texManager->AddTexture("WoodBox", "Texture.tga");
	texManager->AddTexture("WoodBox", "Texture.tga");
	texManager->AddTexture("Ghost", "Ghost.tga");
	texManager->AddTexture("PacManBottom", "PacManBottom.tga");
	texManager->AddTexture("PacManTop", "PacManTop.tga");

	// Load Shaders
	std::vector<D3D11_INPUT_ELEMENT_DESC>* shaderInterface = new std::vector<D3D11_INPUT_ELEMENT_DESC>();

	shaderInterface->push_back({ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 });
	shaderInterface->push_back({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 });
	shaderInterface->push_back({ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 });
	shaderInterface->push_back({ "IPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0,	D3D11_INPUT_PER_INSTANCE_DATA,	1 });
	shaderInterface->push_back({ "IROT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 12,	D3D11_INPUT_PER_INSTANCE_DATA,	1 });
	shaderInterface->push_back({ "ISCA", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 24, D3D11_INPUT_PER_INSTANCE_DATA, 1 });
	shaderInterface->push_back({ "ITEX", 0, DXGI_FORMAT_R32_UINT, 1, 36, D3D11_INPUT_PER_INSTANCE_DATA, 1 });

	shdManager->AddShader("CP", FRAGMENT_SHADER, "CelPixelShader.fx", "PS_Main", "ps_4_0");
	shdManager->AddShader("PS", FRAGMENT_SHADER, "StandardPixelShader.fx", "PS_Main", "ps_4_0");
	shdManager->AddShader("GV", VERTEX_SHADER, "GhostVertexShader.fx", "VS_Main", "vs_4_0", shaderInterface);
	shdManager->AddShader("VS", VERTEX_SHADER, "StandardVertexShader.fx", "VS_Main", "vs_4_0", shaderInterface);

	// Add Objects
	char* entityShaders[2] = { "VS", "PS" };

	entManager->AddEntity("Player", meshManager->GetMesh("Ghost"), "GV", "PS");

	ghost = entManager->GetEntity("Player");

	entManager->AddEntity("PacmanTop", meshManager->GetMesh("PacManTop"), "VS", "PS");
	entManager->AddEntity("PacmanBottom", meshManager->GetMesh("PacManBottom"), "VS", "PS");

	float mapScale = 3.0f;

	for (int i = 0; i < 21; i++)
	{
		for (int j = 0; j < 19; j++)
		{
			std::stringstream ss;
			std::stringstream ss2;

			std::string s;
			std::string s2;

			char part1 = (char)(i + 48);
			char part2 = (char)(j + 48);

			std::string pre = "X";
			std::string mid = "Y";

			ss << part1;
			ss >> s;

			ss2 << part2;
			ss2 >> s2;
			std::string* finalName = new std::string(pre + s + mid + s2);

			char* name = (char *)(*finalName).c_str();

			if (map[i][j] == 1)
			{
				// this is a cube
				entManager->AddEntity(name, meshManager->GetMesh("cube"), "VS", "PS");
				
				Entity* curBox = entManager->GetEntity(name);

				curBox->SetPosition({ (float)j * mapScale, 0.5f, (float)i * mapScale });
				curBox->SetScale({ mapScale, mapScale, mapScale });
				curBox->SetCollidable(true);
				
				mapObjects.push_back(entManager->GetEntity(name));
			}
			/**/
			else if (map[i][j] == 2)
			{
				// normal pill
				entManager->AddEntity(name, meshManager->GetMesh("sphere"), "VS", "PS");

				Entity* curPill = entManager->GetEntity(name);

				curPill->SetPosition({ (float)j * mapScale, 0.5f, (float)i * mapScale });
				curPill->SetScale({ 0.35f, 0.35f, 0.35f });
				curPill->SetCollidable(true);
				curPill->SetTexture(texManager->GetTextureIndex("PacManBottom"));

				Pill* p = new Pill();
				p->entity = curPill;
				p->powerPill = false;
				p->visible = true;
				p->name = name;
				p->x = j;
				p->z = i;

				pacCollectables.push_back(p);
			}
			else if (map[i][j] == 3)
			{
				// power pill
				entManager->AddEntity(name, meshManager->GetMesh("sphere"), "VS", "PS");

				Entity* curPill = entManager->GetEntity(name);

				curPill->SetPosition({ (float)j * mapScale, 0.5f, (float)i * mapScale });
				curPill->SetScale({ 0.5f, 0.5f, 0.5f });
				curPill->SetCollidable(true);
				curPill->SetTexture(texManager->GetTextureIndex("PacManBottom") + 1);

				Pill* p = new Pill();
				p->entity = curPill;
				p->powerPill = true;
				p->visible = true;
				p->name = name;
				p->x = j;
				p->z = i;

				pacCollectables.push_back(p);
			}

			name = NULL;
		}
	}

	entManager->AddEntity("Floor", meshManager->GetMesh("cube"), "VS", "PS");
	entManager->GetEntity("Floor")->SetPosition({ 28, -1.5f, 31 });
	entManager->GetEntity("Floor")->SetScale({ 19.0f * 3.0f, 1.0f, 21.0f * 3.0f});

	pacman.top = entManager->GetEntity("PacmanTop");
	pacman.bottom = entManager->GetEntity("PacmanBottom");
	pacman.animating = true;

	pacman.top->SetCollidable(true);
	pacman.bottom->SetCollidable(true);

	//entManager->AddEntity("1", meshManager->GetMesh("cube"), "VS", "PS");

	ghost->SetPosition(mapObjects[0]->GetPosition());
	ghost->SetPosition({ 27.0f, 0, 30.0f });
	ghost->SetScale({ 0.1f, 0.1f, 0.1f });
	ghost->SetRotation({ ghost->GetRotation().x, ghost->GetRotation().y + (DirectX::XM_PI / 2.0f), ghost->GetRotation().z });

	ghost->SetCollidable(true);

	pacman.SetPosition(27, 0.5f, 36);

	pacman.SetScale(0.25f, 0.25f, 0.25f);

	entManager->GetEntity("PacmanTop")->SetTexture(texManager->GetTextureIndex("PacmanTop"));
	entManager->GetEntity("PacmanBottom")->SetTexture(texManager->GetTextureIndex("PacmanBottom"));
	entManager->GetEntity("Player")->SetTexture(texManager->GetTextureIndex("Ghost"));
	
	gameCamera = engine->GetDXManager()->GetCamera();

	gameCamera->LockedEntity = ghost;

	gameCamera->SetDistance(5.0f);

	gameCamera->RotateBy((DirectX::XM_PI / 12.0f), 0, 0);
	gameCamera->MoveUp(0.5f);

	optimumCameraY = gameCamera->y;

	Rotate(0, DirectX::XM_PI / 2.0f);

	engine->OnRender = OnRender;

	setlocale(LC_ALL, "");

	spriteFont.reset(new DirectX::SpriteFont(engine->GetDXManager()->GetDevice(), L"pixelfont.spritefont"));
}

void OnRender()
{
	using namespace DirectX;

	DirectX::CommonStates* cs = new DirectX::CommonStates(engine->GetDXManager()->GetDevice());

	DirectX::SpriteBatch* spriteBatch;

	spriteBatch = new DirectX::SpriteBatch(engine->GetDXManager()->GetDeviceContext());

	spriteBatch->Begin(DirectX::SpriteSortMode::SpriteSortMode_Immediate, cs->Opaque(), NULL, cs->DepthDefault());

	XMFLOAT2 pos(20, 20);
	XMFLOAT2 pos2(20, 40);
	XMFLOAT3 color(1, 1, 1);
	XMFLOAT2 origin(0, 0);
	XMFLOAT2 scale(1, 1);

	std::wstring str = L"Score: " + std::to_wstring(curScore);

	std::wstring str2 = L"Lives: " + std::to_wstring(pacman.lives);

	spriteFont->DrawString(spriteBatch, str.c_str(), XMLoadFloat2(&pos), XMLoadFloat3(&color), 0.0f, XMLoadFloat2(&origin), XMLoadFloat2(&scale));
	spriteFont->DrawString(spriteBatch, str2.c_str(), XMLoadFloat2(&pos2), XMLoadFloat3(&color), 0.0f, XMLoadFloat2(&origin), XMLoadFloat2(&scale));

	spriteBatch->End();

	delete spriteBatch;
	spriteBatch = NULL;
}

void MovePacman(Pacman* p)
{
	switch (p->curDirection)
	{
	case Direction::LEFT:
		p->SetPosition(p->GetPosition().x - 0.1f, p->GetPosition().y, p->GetPosition().z);
		break;
	case Direction::RIGHT:
		p->SetPosition(p->GetPosition().x + 0.1f, p->GetPosition().y, p->GetPosition().z);
		break;
	case Direction::FORWARD:
		p->SetPosition(p->GetPosition().x, p->GetPosition().y, p->GetPosition().z + 0.1f);
		break;
	case Direction::BACKWARD:
		p->SetPosition(p->GetPosition().x, p->GetPosition().y, p->GetPosition().z - 0.1f);
		break;
	}
}

GameState CheckPacPlayerCollision()
{
	DirectX::XMFLOAT3 dist = pacman.top->GetPosition() - ghost->GetPosition();
	float mag = Magnitude(dist);

	if (mag <= 3.0f)
	{
		if (pacman.top->GetBoundingBox() != NULL && ghost->visible)
		{
			bool colliding = Collision::CheckCollision(ghost->GetBoundingBox(), pacman.top->GetBoundingBox());

			if (colliding)
			{
				pacman.lives--;

				pacman.SetPosition(27, 0.5f, 36);
				pacman.curDirection = Direction::LEFT;

				if (pacman.lives == 0)
				{
					return GameState::QUIT;// Change this to GameOver once GameOver screen ready
				}
			}
		}
	}
}

void ChangePacDirection(Pacman* p)
{
	double xPosF = (double)p->GetPosition().x / 3.0;
	double zPosF = (double)p->GetPosition().z / 3.0;

	int xPos = (int)(ceil(xPosF - 0.5f));
	int zPos = (int)(ceil(zPosF - 0.5f));

	std::pair<int, Direction> preferredDir = std::make_pair(-1, p->curDirection);
	std::pair<int, Direction> backupDir = std::make_pair(-1, p->curDirection);

	std::vector<Direction> allowedDir;

	for (int i = 0; i < pacCollectables.size(); i++)
	{
		Entity* curPill = pacCollectables[i]->entity;

		DirectX::XMFLOAT3 dist = pacman.top->GetPosition() - curPill->GetPosition();
		float mag = Magnitude(dist);

		if (mag <= 3.0f && curPill->visible)
		{
			if (pacman.top->GetBoundingBox() != NULL && curPill->visible)
			{
				bool colliding = Collision::CheckCollision(curPill->GetBoundingBox(), pacman.top->GetBoundingBox());

				if (colliding)
				{
					curScore += 1000;

					if (pacCollectables[i]->powerPill)
					{
						curScore += 500;
					}

					pacCollectables[i]->visible = false;
				}
			}
		}
	}

	if ((fabs(xPos - xPosF) < 0.001f || abs((xPos + 1) - xPosF) < 0.001f) && (abs(zPos - zPosF) < 0.001f || fabs((zPos + 1) - zPosF) < 0.001f))
	{
		// check direction change here
		if (map[zPos][xPos - 1] != 1 && p->curDirection != Direction::RIGHT)
		{
			//left
			allowedDir.push_back(Direction::LEFT);
		}
		if (map[zPos][xPos + 1] != 1 && p->curDirection != Direction::LEFT)
		{
			//right
			allowedDir.push_back(Direction::RIGHT);
		}
		if (map[zPos + 1][xPos] != 1 && p->curDirection != Direction::BACKWARD)
		{
			//forward
			allowedDir.push_back(Direction::FORWARD);
		}
		if (map[zPos - 1][xPos] != 1 && p->curDirection != Direction::FORWARD)
		{
			//back
			allowedDir.push_back(Direction::BACKWARD);
		}

		if (allowedDir.size() > 1)
		{
			//check valid directions for ghost/player
			for (int i = 0; i < allowedDir.size(); i++)
			{
				int xInc = allowedDir[i] == Direction::LEFT ? -1 : (allowedDir[i] == Direction::RIGHT ? 1 : 0);
				int zInc = allowedDir[i] == Direction::FORWARD ? 1 : (allowedDir[i] == Direction::BACKWARD ? -1 : 0);

				int curX = xPos + xInc;
				int curZ = zPos + zInc;

				while (map[curZ][curX] != 1) // go until a wall (line of sight)
				{
					if (GetPill(curX, curZ) != NULL)
					{
						if (GetPill(curX, curZ)->visible)
						{
							if (map[curZ][curX] == 3)
							{
								if (preferredDir.first != -1)
								{
									backupDir = preferredDir;
									break;
								}

								preferredDir = std::make_pair(3, allowedDir[i]);
							}
							else if (map[curZ][curX] == 2)
							{
								if (preferredDir.first == 3 && backupDir.first != 3)
								{
									backupDir = std::make_pair(2, allowedDir[i]);
									break;
								}
								else if (preferredDir.first == -1)
								{
									preferredDir = std::make_pair(2, allowedDir[i]);
								}
							}
							else if (map[curZ][curX] == 0 && backupDir.first == -1)
							{
								backupDir = std::make_pair(0, allowedDir[i]);
							}
						}
					}
					curX += xInc;
					curZ += zInc;
				}

				if (preferredDir.first == -1 && backupDir.first == -1)
				{
					// couldn't find a direction to travel
					int firstIndex = rand() % allowedDir.size();

					preferredDir.second = allowedDir[firstIndex];

					int index = rand() % allowedDir.size();

					while (index == firstIndex)
					{
						index = rand() % allowedDir.size();
					}

					backupDir.second = allowedDir[index];
				}

				//check preferredDir and backupDir for player (so to avoid)

				curX = xPos;
				curZ = zPos;

				while (map[curZ][curX] != 1)
				{
					float xPlayerPosF = p->GetPosition().x / 3.0f;
					float zPlayerPosF = p->GetPosition().z / 3.0f;

					int xPlayerPos = (int)(round(xPlayerPosF - 0.5f));
					int zPlayerPos = (int)(round(zPlayerPosF - 0.5f));

					if (curZ == zPlayerPos && curX == xPlayerPos)
					{
						// player found
						preferredDir = backupDir;
					}

					curX += xInc;
					curZ += zInc;
				}
			}

			RotatePac(p->curDirection, preferredDir.second);

			p->curDirection = preferredDir.second;
		}
		else if (allowedDir.size() != 0)
		{
			RotatePac(p->curDirection, allowedDir[0]);

			p->curDirection = allowedDir[0];
		}
		else
		{
			RotatePac(p->curDirection, preferredDir.second);

			p->curDirection = preferredDir.second;
		}
	}
}

void RotatePac(Direction prevDir, Direction newDir)
{
	if (prevDir == Direction::LEFT)
	{
		if (newDir == Direction::BACKWARD)
		{
			pacman.top->RotateBy(0, DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, DirectX::XM_PIDIV4, 0);
		}
		else if (newDir == Direction::FORWARD)
		{
			pacman.top->RotateBy(0, -DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, -DirectX::XM_PIDIV4, 0);
		}
	}
	else if (prevDir == Direction::RIGHT)
	{
		if (newDir == Direction::BACKWARD)
		{
			pacman.top->RotateBy(0, -DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, -DirectX::XM_PIDIV4, 0);
		}
		else if (newDir == Direction::FORWARD)
		{
			pacman.top->RotateBy(0, DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, DirectX::XM_PIDIV4, 0);
		}
	}
	else if (prevDir == Direction::FORWARD)
	{
		if (newDir == Direction::LEFT)
		{
			pacman.top->RotateBy(0, DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, DirectX::XM_PIDIV4, 0);
		}
		else if (newDir == Direction::RIGHT)
		{
			pacman.top->RotateBy(0, -DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, -DirectX::XM_PIDIV4, 0);
		}
	}
	else if (prevDir == Direction::BACKWARD)
	{
		if (newDir == Direction::LEFT)
		{
			pacman.top->RotateBy(0, -DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, -DirectX::XM_PIDIV4, 0);
		}
		else if (newDir == Direction::RIGHT)
		{
			pacman.top->RotateBy(0, DirectX::XM_PIDIV4, 0);
			pacman.bottom->RotateBy(0, DirectX::XM_PIDIV4, 0);
		}
	}
}

Pill* GetPill(int x, int z)
{
	for (int i = 0; i < pacCollectables.size(); i++)
	{
		if (pacCollectables[i]->x == x && pacCollectables[i]->z == z)
		{
			return pacCollectables[i];
		}
	}

	return NULL;
}

void GamePadUp(int gamepadIndex, GamePadInput keyPressed)
{
	int i = 12;

	if (keyPressed == BTN_SELECT)
	{
		engine->SetGameState(GameState::QUIT);
	}
}

void GamePadDown(int gamepadIndex, GamePadInput keyPressed, float value)
{
	int i = 12;
	/*
	*/

	if (keyPressed == LEFT_STICK_X)
	{
		Move(value == 0 ? 1.0f : value, 0);
	}
	
	if (keyPressed == LEFT_STICK_Y)
	{
		Move(0, value == 0 ? 1.0f : value);
	}

	if (keyPressed == RIGHT_STICK_X)
	{
		float val = value == 0 ? 1.0f : value;

		Rotate(0, ROT_SPEED_MULTI * val);
	}

	if (keyPressed == RIGHT_STICK_Y)
	{
		float val = value == 0 ? 1.0f : value;

		Rotate(ROT_SPEED_MULTI * val, 0);
	}
}

void Move(float x, float y)
{
	float xPos;
	float zPos;

	xPos = sin(ghost->GetRotation().y - (DirectX::XM_PI / 2.0f)) * SPEED_MULTI * (y == 0 ? x : y);
	zPos = cos(ghost->GetRotation().y - (DirectX::XM_PI / 2.0f)) * SPEED_MULTI * (y == 0 ? x : y);
	
	if (y == 0)
	{
		float tmp = xPos;
		xPos = zPos;
		zPos = -tmp;
	}

	bool foundCollision = false;
	
	for (int i = 0; i < mapObjects.size(); i++)
	{
		Entity* curWall = mapObjects[i];

		DirectX::XMFLOAT3 dist = curWall->GetPosition() - ghost->GetPosition();
		float mag = Magnitude(dist);

		if (mag <= 3.0f)
		{
			int i = 0;

			if (curWall->GetBoundingBox() != NULL)
			{
				bool colliding = *ghost ^ *curWall;

				if (colliding)
				{
					foundCollision = true;

					ghost->MoveX(xPos);
					if (*ghost ^ *curWall)
					{
						ghost->MoveX(-xPos);
					}

					ghost->MoveZ(zPos);
					if (*ghost ^ *curWall)
					{
						ghost->MoveZ(-zPos);
					}
				}
			}
		}
	}

	if (!foundCollision)
	{
		ghost->MoveX(xPos);
		ghost->MoveZ(zPos);
	}
}

float Magnitude(DirectX::XMFLOAT3 vec)
{
	return pow(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2), 0.5f);
}

DirectX::XMFLOAT3 operator+(DirectX::XMFLOAT3 one, DirectX::XMFLOAT3 two)
{
	return DirectX::XMFLOAT3(one.x + two.x, one.y + two.y, one.z + two.z);
}
DirectX::XMFLOAT3 operator-(DirectX::XMFLOAT3 one, DirectX::XMFLOAT3 two)
{
	return DirectX::XMFLOAT3(one.x - two.x, one.y - two.y, one.z - two.z);
}

void TryResetCamera()
{
	float curY = gameCamera->y;

	float curRot = asin(gameCamera->y / gameCamera->GetDistance());
	float newRot = asin(optimumCameraY / gameCamera->GetDistance());

	gameCamera->rotX = newRot;
	gameCamera->y = optimumCameraY;

	ManageCameraPosition();

	bool foundCollision = false;

	for (int i = 0; i < mapObjects.size(); i++)
	{
		Entity* curWall = mapObjects[i];

		DirectX::XMFLOAT3 dist = gameCamera->GetPosition() - curWall->GetPosition();
		float mag = Magnitude(dist);

		if (mag <= 3.0f)
		{
			int i = 0;

			if (gameCamera->GetBoundingBox() != NULL)
			{
				bool colliding = Collision::CheckCollision(curWall->GetBoundingBox(), gameCamera->GetBoundingBox());

				if (colliding)
				{
					foundCollision = true;
				}
			}
		}
	}

	if (foundCollision)
	{
		gameCamera->rotX = curRot;
		gameCamera->y = curY;

		ManageCameraPosition();
	}
}

void CameraCollision()
{
	bool foundCollision = false;

	for (int i = 0; i < mapObjects.size(); i++)
	{
		Entity* curWall = mapObjects[i];

		DirectX::XMFLOAT3 dist = gameCamera->GetPosition() - curWall->GetPosition();
		float mag = Magnitude(dist);

		if (mag <= 3.0f)
		{
			int i = 0;

			if (gameCamera->GetBoundingBox() != NULL)
			{
				bool colliding = Collision::CheckCollision(curWall->GetBoundingBox(), gameCamera->GetBoundingBox());

				if (colliding)
				{
					foundCollision = true;

					bool stillColliding = true;

					while (stillColliding)
					{
						float curRot = asin(gameCamera->y / gameCamera->GetDistance());
						float newRot = asin((gameCamera->y - 0.1f) / gameCamera->GetDistance());

						float rotDist = newRot - curRot;

						gameCamera->MoveUp(0.1f);

						if (gameCamera->y > gameCamera->GetDistance())
						{
							gameCamera->y = gameCamera->GetDistance();
						}

						gameCamera->RotateBy(-rotDist, 0, 0);

						ManageCameraPosition();

						stillColliding = Collision::CheckCollision(curWall->GetBoundingBox(), gameCamera->GetBoundingBox());
					}
				}
			}
		}
	}
}

void Rotate(float x, float y)
{
	ghost->RotateBy(0, y, 0);

	gameCamera->RotateBy(x, 0, 0);
}

DWORD lastTickCount = 0;

Global::Enums::GameState Run()
{
	if (lastTickCount == 0)
	{
		lastTickCount = GetTickCount();
	}
	else
	{
		DWORD curTickCount = GetTickCount();

		float diff = curTickCount - lastTickCount;

		float fps = 1000.0f / diff;

		char test[200];
		sprintf_s(test, "FPS: %f\r\n", fps);

		OutputDebugString(test);

		lastTickCount = curTickCount;
	}
	
	GameState state = GameState::RUNNING;

	EntityManager* entMgr = engine->GetDXManager()->GetEntityManager();

	//gameCamera->RotateBy(-0.01f, 0, 0);

	float leftVib = 0;
	float rightVib = 0;

	DirectX::XMFLOAT3 diff = ghost->GetPosition() - pacman.GetPosition();

	if (Magnitude(diff) < 10.0f)
	{
		leftVib = 65535.0f / Magnitude(diff);
		rightVib = 65535.0f / Magnitude(diff);

		if (diff.x == 0)
		{
			diff.x = 1;
		}
		if (diff.y == 0)
		{
			diff.y = 1;
		}
		if (diff.z == 0)
		{
			diff.z = 1;
		}

		leftVib = (65535.0f / diff.x) - leftVib;
		leftVib = leftVib - (65535.0f / diff.x);
	}

	if (leftVib < 0)
	{
		leftVib = 0;
	}
	if (rightVib < 0)
	{
		rightVib = 0;
	}

	XINPUT_VIBRATION vibration;
	vibration.wLeftMotorSpeed = leftVib;
	vibration.wRightMotorSpeed = rightVib;

	XInputSetState((DWORD)0, &vibration);
	
	pacman.Animate();
	//entMgr->GetEntity("PacmanTop")->SetRotation({ entMgr->GetEntity("PacmanTop")->GetRotation().x + sin(entMgr->GetEntity("PacmanTop")->GetRotation().x + 0.001f) / 2.0f, entMgr->GetEntity("PacmanTop")->GetRotation().y, entMgr->GetEntity("PacmanTop")->GetRotation().z });

	//entMgr->GetEntity("PacmanBottom")->SetRotation({ entMgr->GetEntity("PacmanBottom")->GetRotation().x, entMgr->GetEntity("PacmanBottom")->GetRotation().y + 0.001f, entMgr->GetEntity("PacmanBottom")->GetRotation().z });

	//Entity* ent = entMgr->GetEntity("1");

	ChangePacDirection(&pacman);
	MovePacman(&pacman);

	CheckPills();
	ManageCameraPosition();
	CameraCollision();
	TryResetCamera();

	state = HandleInput();

	GameState s = CheckPacPlayerCollision();

	if (s == GameState::QUIT)
	{
		return s;
	}

	return state;
}

void ManageCameraPosition()
{
	float x, z = 0;

	x = sin(ghost->GetRotation().y - (DirectX::XM_PI / 2.0f));
	z = cos(ghost->GetRotation().y - (DirectX::XM_PI / 2.0f));

	gameCamera->Rotate(gameCamera->rotX, ghost->GetRotation().y - (DirectX::XM_PI / 2.0f), ghost->GetRotation().z);


	gameCamera->x = (ghost->GetPosition().x - (x * gameCamera->GetDistance())) / 2.0f;
	gameCamera->z = (ghost->GetPosition().z - (z * gameCamera->GetDistance())) / 2.0f;
}

void Exit()
{
	XINPUT_VIBRATION vibration;
	vibration.wLeftMotorSpeed = 0;
	vibration.wRightMotorSpeed = 0;

	XInputSetState((DWORD)0, &vibration);
}

Global::Enums::GameState HandleInput()
{
	if (engine->KeyPress(VK_ESCAPE))
	{
		return GameState::QUIT;
	}

	if (engine->KeyPress('W'))
	{
		Move(0, 1);
	}
	if (engine->KeyPress('S'))
	{
		Move(0, -1);
	}
	if (engine->KeyPress('A'))
	{
		Move(-1, 0);
	}
	if (engine->KeyPress('D'))
	{
		Move(1, 0);
	}

	if (engine->KeyPress(VK_UP))
	{
		gameCamera->MoveUp(0.1f);
	}
	if (engine->KeyPress(VK_DOWN))
	{
		gameCamera->MoveUp(-0.1f);
	}

	if (engine->KeyPress(VK_PAUSE))
	{
		engine->GetDXManager()->ToggleFullscreen();
	}

	if (engine->KeyPress('Q'))
	{
		Rotate(0, -ROT_SPEED_MULTI);
	}
	if (engine->KeyPress('E'))
	{
		Rotate(0, ROT_SPEED_MULTI);
	}
}