Texture2DArray _colorMap : register(t0);
SamplerState _colorMapSampler : register(s0);

cbuffer cbCameraPosition : register(b0)
{
	float3 cameraPosition;
}

struct PS_Input
{
	float4 pos : SV_POSITION;
	float3 norm : NORMAL;
	float3 tex0 : TEXCOORD0;
};

float4 PS_Main(PS_Input frag) : SV_TARGET
{
	return _colorMap.Sample(_colorMapSampler, frag.tex0);
}