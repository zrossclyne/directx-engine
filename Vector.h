#pragma once

class Vector
{
public:
	Vector();
	Vector(float x, float y, float z);
	~Vector();

	float x;
	float y;
	float z;
};
