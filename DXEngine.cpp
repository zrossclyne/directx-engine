#include "DXEngine.h"

LRESULT CALLBACK DXEngine::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT paintStruct;
	HDC hDC;

	switch (msg)
	{
	case WM_PAINT:
		hDC = BeginPaint(hwnd, &paintStruct);
		EndPaint(hwnd, &paintStruct);
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	return 0;
}


DXEngine::DXEngine(char* className, char* windowName, HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{
	HMONITOR primaryMonitor = MonitorFromWindow(NULL, MONITOR_DEFAULTTOPRIMARY);
	MONITORINFO primaryMonitorInfo;

	primaryMonitorInfo.cbSize = sizeof(MONITORINFO);

	GetMonitorInfo(primaryMonitor, &primaryMonitorInfo);

	RECT monitorRect = primaryMonitorInfo.rcMonitor;

	ushort winWidth = monitorRect.right - monitorRect.left;
	ushort winHeight = monitorRect.bottom - monitorRect.top;

	DisableDirectInput();

	this->winWidth = winWidth;
	this->winHeight = winHeight;

	this->className = className;
	this->windowName = windowName;

	this->hInstance = hInstance;
	this->prevInstance = prevInstance;
	this->cmdLine = cmdLine;
	this->cmdShow = cmdShow;
}



DXEngine::~DXEngine()
{
}

int DXEngine::Start(void (*preLoopConfig) (void), GameState(*f) (void), void(*exit) (void))
{
	gameState = GameState::LOADING;

	UNREFERENCED_PARAMETER(prevInstance);
	UNREFERENCED_PARAMETER(cmdLine);

	WNDCLASSEX wndClass = { 0 };

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = this->WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = className;

	if (!RegisterClassEx(&wndClass))
	{
		return -1;
	}

	RECT rc = { 0, 0, winWidth, winHeight };

	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

	HWND hwnd = CreateWindowA(className, windowName, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);

	if (!hwnd)
	{
		return -1;
	}

	gameWindow = hwnd;

	ShowWindow(hwnd, cmdShow);
	ShowCursor(cursorVisible);
	//std::auto_ptr<DXManager> directX(new DXManager(winWidth, winHeight, hInstance, hwnd));

	bool result = true;

	if (result == false)
	{
		return -1;
	}

	MSG msg = { 0 };

	//ShowCursor(false);

	dxManager = new DXManager(hInstance, hwnd);
	
	gameState = GameState::RUNNING;

	
	preLoopConfig();

	dxManager->OnRender = OnRender;

	int elapsed = 0;
	int frames = 0;

	while (msg.message != WM_QUIT && gameState != GameState::QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		
		if (cursorLocked && GetFocus() == hwnd)
		{
			RECT winPos;
			
			GetWindowRect(hwnd, &winPos);

			SetCursorPos((winWidth / 2.0f) + winPos.left, (winHeight / 2.0f) + winPos.top);
		}


		int i = GetTickCount();
		dxManager->DrawInstanced();
		elapsed += GetTickCount() - i;
		frames++;
		
		// do your stuff here (run)
		gameState = f();

		if (elapsed > 1000)
		{
			//second
			frames = 0;
			elapsed = 0;
		}

		for (int i = 0; i < xInputs.size(); i++)
		{
			CheckGamePad(i);
		}
	}
	
	msg.message = WM_QUIT;

	exit();

	dxManager->DisableFullscreen();

	return static_cast<int>(msg.wParam);
}

void DXEngine::OnRenderPriv()
{
	if (OnRender != NULL)
	{
		OnRender();
	}
}

void DXEngine::LockCursorPosition(bool val)
{
	cursorLocked = val;
}

void DXEngine::SetCursorVisibility(bool val)
{
	cursorVisible = val;
}

void DXEngine::SetGameState(GameState state)
{
	gameState = state;
}

ushort DXEngine::GetWinHeight()
{
	return winHeight;
}

ushort DXEngine::GetWinWidth()
{
	return winWidth;
}

void DXEngine::Output(LPCSTR message)
{
	OutputDebugString(message);
	OutputDebugString("\n");
}

DXManager* DXEngine::GetDXManager()
{
	return dxManager;
}

SHORT DXEngine::KeyPress(int keyPress)
{
	if (GetFocus() == gameWindow)
	{
		return GetAsyncKeyState(keyPress);
	}
	else
	{
		return 0;
	}
}

void DXEngine::EnableXInput(short numGamepads)
{
	for (int i = 0; i < numGamepads; i++)
	{
		xInputs.emplace_back();
	}

	xInput = true;
}

void DXEngine::DisableXInput()
{
	xInput = false;
}

void DXEngine::EnableDirectInput(short numGamepads)
{
	directInput = true;
}

void DXEngine::DisableDirectInput()
{
	directInput = false;
}

void DXEngine::CheckGamePad(short index)
{
	// Using XInput for checking GamePad control
	unsigned long result = XInputGetState((DWORD)index, &xInputs[index].state);

	if (result == ERROR_DEVICE_NOT_CONNECTED)
	{
		return;
	}
	else if (result != ERROR_SUCCESS)
	{
		return;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
	{
		PostQuitMessage(0);
	}

	float lXDir = ((xInputs[index].state.Gamepad.sThumbLX - 1713.0f) / 30000.0f);
	float lYDir = ((xInputs[index].state.Gamepad.sThumbLY + 1815.0f) / 30000.0f);
	float rXDir = ((xInputs[index].state.Gamepad.sThumbRX - 136.0f) / 30000.0f);
	float rYDir = ((xInputs[index].state.Gamepad.sThumbRY + 1056.0f) / 30000.0f);

	WORD leftMotorSpeed = 0;
	WORD rightMotorSpeed = 0;

	float leftTriggerVal = (float)xInputs[index].state.Gamepad.bLeftTrigger;
	float rightTriggerVal = (float)xInputs[index].state.Gamepad.bRightTrigger;

	#pragma region Button Events

	if (abs(lXDir) > 0.0f + this->deadZone)
	{
		SendKeyDown(index, LEFT_STICK_X, lXDir);
	}
	else
	{
		SendKeyUp(index, LEFT_STICK_X);
	}

	if (abs(lYDir) > 0.0f + this->deadZone)
	{
		SendKeyDown(index, LEFT_STICK_Y, lYDir);
	}
	else
	{
		SendKeyUp(index, LEFT_STICK_Y);
	}

	if (abs(rXDir) > 0.0f + this->deadZone)
	{
		SendKeyDown(index, RIGHT_STICK_X, rXDir);
	}
	else
	{
		SendKeyUp(index, RIGHT_STICK_X);
	}

	if (abs(rYDir) > 0.0f + this->deadZone)
	{
		SendKeyDown(index, RIGHT_STICK_Y, rYDir);
	}
	else
	{
		SendKeyUp(index, RIGHT_STICK_Y);
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER && !xInputs[index].leftBumper)
	{
		SendKeyDown(index, BTN_LB, 1);
		xInputs[index].leftBumper = true;
	}
	else if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)
	{
		xInputs[index].leftBumper = false;
		SendKeyUp(index, BTN_LB);
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER && !xInputs[index].rightBumber)
	{
		xInputs[index].rightBumber = true;
		SendKeyDown(index, BTN_RB, 1);
	}
	else if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)
	{
		xInputs[index].rightBumber = false;
		SendKeyUp(index, BTN_RB);
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_START && !xInputs[index].start)
	{
		xInputs[index].start = true;
		SendKeyDown(index, BTN_START, 1);
	}
	else if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_START)
	{
		xInputs[index].start = false;
		SendKeyUp(index, BTN_START);
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK && !xInputs[index].select)
	{
		xInputs[index].select = true;
		SendKeyDown(index, BTN_SELECT, 1);
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) && xInputs[index].select)
	{
		xInputs[index].select = false;
		SendKeyUp(index, BTN_SELECT);
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT && !xInputs[index].dPadRight)
	{
		SendKeyDown(index, D_PAD_RIGHT, 1);
		xInputs[index].dPadRight = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) && xInputs[index].dPadRight)
	{
		SendKeyUp(index, D_PAD_RIGHT);
		xInputs[index].dPadRight = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT && !xInputs[index].dPadLeft)
	{
		SendKeyDown(index, D_PAD_LEFT, 1);
		xInputs[index].dPadLeft = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) && xInputs[index].dPadLeft)
	{
		SendKeyUp(index, D_PAD_LEFT);
		xInputs[index].dPadLeft = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP && !xInputs[index].dPadUp)
	{
		SendKeyDown(index, D_PAD_UP, 1);
		xInputs[index].dPadUp = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) && xInputs[index].dPadUp)
	{
		SendKeyUp(index, D_PAD_UP);
		xInputs[index].dPadUp = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN && !xInputs[index].dPadDown)
	{
		SendKeyDown(index, D_PAD_DOWN, 1);
		xInputs[index].dPadDown = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) && xInputs[index].dPadDown)
	{
		SendKeyUp(index, D_PAD_DOWN);
		xInputs[index].dPadDown = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_A && !xInputs[index].a)
	{
		SendKeyDown(index, BTN_A, 1);
		xInputs[index].a = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_A) && xInputs[index].a)
	{
		SendKeyUp(index, BTN_A);
		xInputs[index].a = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_B && !xInputs[index].b)
	{
		SendKeyDown(index, BTN_B, 1);
		xInputs[index].b = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_B) && xInputs[index].b)
	{
		SendKeyUp(index, BTN_B);
		xInputs[index].b = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_X && !xInputs[index].x)
	{
		SendKeyDown(index, BTN_X, 1);
		xInputs[index].x = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_X) && xInputs[index].x)
	{
		SendKeyUp(index, BTN_X);
		xInputs[index].x = false;
	}

	if (xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_Y && !xInputs[index].y)
	{
		SendKeyDown(index, BTN_Y, 1);
		xInputs[index].y = true;
	}
	else if (!(xInputs[index].state.Gamepad.wButtons & XINPUT_GAMEPAD_Y) && xInputs[index].y)
	{
		SendKeyUp(index, BTN_Y);
		xInputs[index].y = false;
	}

	if (xInputs[index].state.Gamepad.bLeftTrigger > 0)
	{
		SendKeyDown(index, BTN_LT, (WORD)(65535.0f * (leftTriggerVal / 255.0f)));
	}
	else
	{
		SendKeyUp(index, BTN_LT);
	}

	if (xInputs[index].state.Gamepad.bRightTrigger> 0)
	{
		SendKeyDown(index, BTN_RT, (WORD)(65535.0f * (leftTriggerVal / 255.0f)));
	}
	else
	{
		SendKeyUp(index, BTN_RT);
	}
	#pragma endregion

	memcpy(&xInputs[index].previousState, &xInputs[index].state, sizeof(XINPUT_STATE)); // copy into previous controller state.
}

void DXEngine::SetDeadZone(float deadZone)
{
	this->deadZone = deadZone;
}

void DXEngine::SendKeyDown(int gamepadIndex, GamePadInput inputPressed, float value)
{
	if (GamePadPressed != NULL)
	{
		GamePadPressed(gamepadIndex, inputPressed, value);
	}
}

void DXEngine::SendKeyUp(int gamepadIndex, GamePadInput inputPressed)
{
	if (GamePadUnpressed != NULL)
	{
		GamePadUnpressed(gamepadIndex, inputPressed);
	}
}

void DXEngine::CheckMouseState()
{
	// Win32 Mouse controls
	POINT point;
	RECT rect;

	GetCursorPos(&point);

	const HWND hwnd = GetDesktopWindow();

	GetWindowRect(hwnd, &rect);

	int middleX = (rect.right) / 2;
	int middleY = (rect.bottom) / 2;

	int diffX = point.x - middleX;
	int diffY = point.y - middleY;

	float newX = 0.0f;
	float newY = 0.0f;

	if (abs(diffX) > 0)
	{
		newY = 0.02f * (abs(diffX) / diffX);
	}
	if (abs(diffY) > 0)
	{
		newX = 0.02f * (abs(diffY) / diffY);
	}

	SetCursorPos(middleX, middleY);
}