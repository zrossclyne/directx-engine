#pragma once
#include "Globals.h"
#include <map>
#include "Entity.h"
#include "Mesh.h"

class EntityManager
{
private:
	std::map<char*, Entity> entities;

public:
	EntityManager();
	~EntityManager();

	void AddEntity(char* name, Mesh* mesh, char* vertexShader, char* fragmentShader);
	void RemoveEntity(Entity* name);

	Entity* GetEntity(char* name);

	std::map<char*, Entity>* GetEntities();
};

