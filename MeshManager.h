#pragma once
#include "Mesh.h"
#include <map>
#include "MeshLoader.h"
class MeshManager
{
public:
	MeshManager();
	~MeshManager();

	void AddMesh(char* name, char* fileName);
	Mesh* GetMesh(char* name);

	std::map<char*, Mesh>* GetMeshes();
private:

	MeshLoader* loader;

	std::map<char*, Mesh> meshes;

};

