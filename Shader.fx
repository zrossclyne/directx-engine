Texture2DArray _colorMap : register(t0);
SamplerState _colorMapSampler : register(s0);

cbuffer cbCameraMatrix : register(b0)
{
	matrix viewProjectionMatrix;
}

struct VS_Input
{
	float4 pos : POSITION;
	float2 tex0 : TEXCOORD0;
	float4 normal : NORMAL;

	float4 instancePos : IPOS;
	float4 instanceRot : IROT;
	float4 instanceSca : ISCA;
	uint instanceTex : ITEX;
};


struct PS_Input
{
	float4 pos : SV_POSITION;
	float3 tex0 : TEXCOORD0;
};

PS_Input VS_Main(VS_Input vertex)
{
	PS_Input vsOut = (PS_Input)0;
	float rotX = vertex.instanceRot.x;
	float rotY = vertex.instanceRot.y;
	float rotZ = vertex.instanceRot.z;

	float4x4 rotationMatrixX = { { 1, 0, 0, 0 }, { 0, cos(rotX), -sin(rotX), 0 }, { 0, sin(rotX), cos(rotX), 0 }, { 0, 0, 0, 1 } };
	float4x4 rotationMatrixY = { { cos(rotY), 0, sin(rotY), 0 }, { 0, 1, 0, 0 }, { -sin(rotY), 0, cos(rotY), 0 }, { 0, 0, 0, 1 } };
	float4x4 rotationMatrixZ = { { cos(rotZ), -sin(rotZ), 0, 0 }, { sin(rotZ), cos(rotZ), 0, 0 }, { 0, 0, 1, 0 }, { 0, 0, 0, 1 } };

	float4x4 scale = { { vertex.instanceSca.x, 0, 0, 0 }, { 0, vertex.instanceSca.y, 0, 0 }, { 0, 0, vertex.instanceSca.z, 0 }, { 0, 0, 0, 1 } };
	
	vsOut.pos = vertex.pos;

	// Perform scale
	vsOut.pos = mul(vsOut.pos, scale);

	//Perform rotation
	vsOut.pos = mul(vsOut.pos, rotationMatrixX);
	vsOut.pos = mul(vsOut.pos, rotationMatrixY);
	vsOut.pos = mul(vsOut.pos, rotationMatrixZ);

	//vsOut.pos = mul(vsOut.pos, worldMatrix);
	vsOut.pos = vsOut.pos + vertex.instancePos;
	/*
	IFLY
	*/
	vsOut.pos = mul(vsOut.pos, viewProjectionMatrix);
	//vsOut.pos = mul(vsOut.pos, projMatrix);
	//vsOut.tex0 = vertex.tex0;// float3 { vertex.tex0.x, vertex.tex0.y, instanceTex };
	vsOut.tex0.x = vertex.tex0.x;
	vsOut.tex0.y = vertex.tex0.y;
	vsOut.tex0.z = vertex.instanceTex;

	return vsOut;

	/*
	PS_Input output;

	float4 nPos = float4(vertex.pos.x * vertex.instanceSca.x, vertex.pos.y * vertex.instanceSca.y, vertex.pos.z * vertex.instanceSca.z, 1);

	vertex.pos.w = 1;
	vertex.instanceSca.w = 1;

//	nPos = mul(vertex.pos, vertex.instanceSca);

	output.pos = nPos + vertex.instancePos;
	output.tex0 = vertex.tex0;

	return output;*/
}

float4 PS_Main(PS_Input frag) : SV_TARGET
{
	return _colorMap.Sample(_colorMapSampler, frag.tex0);
}