#include "MeshManager.h"


MeshManager::MeshManager()
{
	loader = new MeshLoader();
}


MeshManager::~MeshManager()
{
}


void MeshManager::AddMesh(char* name, char* fileName)
{
	meshes.insert(std::make_pair(name, loader->Load(fileName)));
}

Mesh* MeshManager::GetMesh(char* name)
{
	return &meshes[name];
}

std::map<char*, Mesh>* MeshManager::GetMeshes()
{
	return &meshes;
}