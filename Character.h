#pragma once
#include "Entity3D.h"
#include <vector>
#include <typeinfo>

class Character : public Entity3D
{
private:
	Entity::components;
public:
	Character();
	~Character();

	void update();
	void render();
};

