#pragma once
#include "Globals.h"

#define XMComparisonAnyTrue(CR)            (((CR) & DirectX::XM_CRMASK_CR6FALSE) != DirectX::XM_CRMASK_CR6FALSE)

struct AxisAlignedBox
{
	DirectX::XMFLOAT3 Center;
	DirectX::XMFLOAT3 Extents;
};

class Collision
{
public:
	Collision();
	~Collision();

	static bool CheckCollision(AxisAlignedBox* volumeA, AxisAlignedBox* volumeB);
};

