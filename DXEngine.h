#pragma once
#include "Globals.h"

#include <XInput.h>

#include "DXManager.h"
#include <memory>

using namespace Global::TypeDefinitions;
using namespace Global::Enums;

struct XInputGamepad
{
	XINPUT_STATE previousState;
	XINPUT_STATE state;

	bool dPadRight;
	bool dPadLeft;
	bool dPadUp;
	bool dPadDown;

	bool leftBumper;
	bool rightBumber;

	bool start;
	bool select;

	bool a;
	bool b;
	bool x;
	bool y;

	bool leftStick;
	bool rightStick;
};

enum GamePadInput
{
	LEFT_STICK_X,
	LEFT_STICK_Y,
	RIGHT_STICK_X,
	RIGHT_STICK_Y,
	D_PAD_UP,
	D_PAD_DOWN,
	D_PAD_LEFT,
	D_PAD_RIGHT,
	BTN_A,
	BTN_B,
	BTN_X,
	BTN_Y,
	BTN_LB,
	BTN_LT,
	BTN_RB,
	BTN_RT,
	BTN_LEFT_STICK,
	BTN_RIGHT_STICK,
	BTN_START,
	BTN_SELECT
};

// Main interface between the Engine and the main Application.
class DXEngine
{
public:
	// Constructors
	DXEngine(char* className, char* windowName, HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow);
	
	~DXEngine();

	// Functions
	int Start(void(*preLoopConfig) (void), GameState(*f) (void), void(*exit) (void));

	ushort GetWinHeight();
	ushort GetWinWidth();

	void Output(LPCSTR message);

	SHORT KeyPress(int keyPress);
	void EnableDirectInput(short numGamepads);
	void DisableDirectInput();

	void EnableXInput(short numGamepads);
	void DisableXInput();

	DXManager* DXEngine::GetDXManager();

	void(*GamePadPressed) (int, GamePadInput, float);
	void(*GamePadUnpressed) (int, GamePadInput);

	void(*OnRender) ();

	void SetDeadZone(float deadZone);

	void SetCursorVisibility(bool val);
	void LockCursorPosition(bool val);

	void SetGameState(GameState state);

private:
	static LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	
	DXManager* dxManager;

	ushort winWidth;
	ushort winHeight;

	// Window variables
	char* className;
	char* windowName;

	bool cursorLocked;
	bool cursorVisible;
	bool directInput;
	bool xInput;

	float deadZone;

	void OnRenderPriv();

	HINSTANCE hInstance;
	HINSTANCE prevInstance;
	LPWSTR cmdLine;

	HWND gameWindow;

	int cmdShow;

	void CheckMouseState();
	void CheckGamePad(short gamepadIndex);

	void SendKeyDown(int, GamePadInput, float);
	void SendKeyUp(int, GamePadInput);

	std::vector<XInputGamepad> xInputs;

	GameState gameState;
};

