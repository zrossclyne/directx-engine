#pragma once
#include "Globals.h"
#include "Shader.h"

// Some people call it a Pixel Shader some call it a Fragment shader (official name)
//		See here: GET LINK FOR REFERENCE HERE
// This allows users to use a term they are more confortable with

class FragmentShader : public Shader
{
private:
	ID3D11PixelShader *shader;

public:
	
	FragmentShader(ID3D11Device *_device, ID3DBlob *buffer);
	~FragmentShader();
	ID3D11PixelShader* GetShader();
};


