#include "Entity.h"


Entity::Entity()
{

}

Entity::Entity(Mesh* mesh, char* vertexShader, char* fragmentShader)
{
	this->mesh = mesh;
	this->shaders[0] = vertexShader;
	this->shaders[4] = fragmentShader;

	this->visible = true;
	this->collidable = false;
}

void Entity::AssignShader(ShaderType type, char* shaderName)
{
	this->shaders[type] = shaderName;
}

char** Entity::GetShaders()
{
	return shaders;
}

void Entity::AssignTexture(char* texture)
{
	this->texture = texture;
}

char* Entity::GetShader(ShaderType type)
{
	return this->shaders[type];
}

char* Entity::GetTexture()
{
	return texture;
}

Mesh* Entity::GetMesh()
{
	return this->mesh;
}

Entity::~Entity()
{
}

DirectX::XMFLOAT3 Entity::GetPosition()
{
	return instanceData.position;
}

DirectX::XMFLOAT3 Entity::GetRotation()
{
	return instanceData.rotation;
}

DirectX::XMFLOAT3 Entity::GetScale()
{
	return instanceData.scale;
}

void Entity::RotateBy(float x, float y, float z)
{
	instanceData.rotation.x += x;
	instanceData.rotation.y += y;
	instanceData.rotation.z += z;
}

void Entity::SetPosition(DirectX::XMFLOAT3 position)
{
	instanceData.position = position;
}

void Entity::SetScale(DirectX::XMFLOAT3 scale)
{
	instanceData.scale = scale;
}

void Entity::SetRotation(DirectX::XMFLOAT3 rotation)
{
	instanceData.rotation = rotation;
}

void Entity::SetTexture(int texIndex)
{
	instanceData.texIndex = texIndex;
}

void Entity::MoveX(float val)
{
	instanceData.position.x += val;
}

void Entity::MoveY(float val)
{
	instanceData.position.y += val;
}

void Entity::MoveZ(float val)
{
	instanceData.position.z += val;
}

Instance* Entity::GetInstanceData()
{
	return &instanceData;
}

void Entity::render()
{

}
void Entity::update()
{

}

std::vector<IComponent*>* Entity::GetComponents()
{
	return &components;
}

void Entity::AddComponent(IComponent* component)
{
	components.push_back(component);
}

AxisAlignedBox* Entity::GetBoundingBox()
{
	if (collidable)
	{
		this->SetCollidable(true);

		return &boundingBox;
	}
	else
	{
		return NULL;
	}
}

void Entity::SetCollidable(bool collidable)
{
	this->collidable = collidable;

	if (collidable)
	{
		boundingBox = mesh->GenerateAABB();

		boundingBox.Center.x *= instanceData.scale.x;
		boundingBox.Center.y *= instanceData.scale.y;
		boundingBox.Center.z *= instanceData.scale.z;

		boundingBox.Center.x += instanceData.position.x;
		boundingBox.Center.y += instanceData.position.y;
		boundingBox.Center.z += instanceData.position.z;


		boundingBox.Extents.x *= instanceData.scale.x;
		boundingBox.Extents.y *= instanceData.scale.y;
		boundingBox.Extents.z *= instanceData.scale.z;
	}
	else
	{
		boundingBox = AxisAlignedBox();
	}
}
