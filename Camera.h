#pragma once

#include "Globals.h"
#include "Vector.h"
#include "Entity.h"

class Camera
{
public:
	Camera();
	~Camera();

	void MoveUp(float val);
	void MoveDown(float val);
	void MoveLeft(float val);
	void MoveRight(float val);

	void MoveForward(float val);
	void MoveBackward(float val);

	void LookAt(Vector location);
	void LookAt(float x, float y, float z);

	void MoveTo(Vector location);
	void MoveTo(float x, float y, float z);

	void Rotate(Vector rotation);
	void Rotate(float roll, float pitch, float yaw);

	void RotateBy(float roll, float pitch, float yaw);

	DirectX::XMFLOAT3 GetPosition();

	float GetDistance();
	void SetDistance(float dist);

	Entity* LockedEntity;

	DirectX::XMFLOAT3 GetLookDirection();

	DirectX::XMMATRIX GetViewProjection();

	AxisAlignedBox* GetBoundingBox();

	float x, y, z;

	float rotX, rotY, rotZ;

private:
	AxisAlignedBox boundingBox;

	float distance;

};

