#include "Character.h"


Character::Character()
{

}


Character::~Character()
{
}

void Character::update()
{
	for each (IComponent* var in components)
	{
		var->update();
	}
}

void Character::render()
{
	for each (IComponent* var in components)
	{
		if (typeid(var) == typeid(IRenderableComponent))
		{
			IRenderableComponent* comp = (IRenderableComponent *)var;

			comp->render();
		}
	}
}