#pragma once
#include <vector>
#include "Globals.h"
#include "Collision.h"
#include "Vertex.h"

class Mesh
{
public:
	Mesh();
	Mesh(std::vector<Vertex3> m_Vertex, std::vector<WORD> m_Index);
	~Mesh();

	void Build(Vertex3* vertices, int vertexCount);

	AxisAlignedBox GenerateAABB();

	std::vector<Vertex3> GetVertices();
	std::vector<WORD> GetIndices();
private:
	std::vector<Vertex3> vertices;
	std::vector<WORD> indices;
};

