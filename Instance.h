#pragma once
#include "Globals.h"

class Instance
{
public:
	Instance();
	~Instance();

	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMFLOAT3 scale;
	int texIndex;

	friend bool operator==(const Instance &left, const Instance &right)
	{
		return (
			left.position.x == right.position.x &&
			left.position.y == right.position.y &&
			left.position.z == right.position.z &&
			left.scale.x == right.scale.x &&
			left.scale.y == right.scale.y &&
			left.scale.z == right.scale.z &&
			left.rotation.x == right.rotation.x &&
			left.rotation.y == right.rotation.y &&
			left.rotation.z == right.rotation.z &&
			left.texIndex == right.texIndex
			);
	}

	friend bool operator!=(const Instance &left, const Instance &right)
	{
		return (
			left.position.x != right.position.x &&
			left.position.y != right.position.y &&
			left.position.z != right.position.z &&
			left.scale.x != right.scale.x &&
			left.scale.y != right.scale.y &&
			left.scale.z != right.scale.z &&
			left.rotation.x != right.rotation.x &&
			left.rotation.y != right.rotation.y &&
			left.rotation.z != right.rotation.z &&
			left.texIndex != right.texIndex
			);
	}

	//float textureID;
};

