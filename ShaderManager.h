#pragma once
#include "Globals.h"
#include <map>
#include "Shader.h"
#include "VertexShader.h"

using namespace Global::Enums;

class ShaderManager
{
private:
	std::map<char*, std::pair<ShaderType, Shader*>> shaders;
	ID3D11Device *_device;

public:
	ShaderManager(ID3D11Device *_device);
	~ShaderManager();

	void AddShader(char* shaderName, ShaderType shaderType, char* shaderPath, char* entryPoint, char* shaderModel, std::vector<D3D11_INPUT_ELEMENT_DESC>* shaderInterface);
	void AddShader(char* shaderName, ShaderType shaderType, char* shaderPath, char* entryPoint, char* shaderModel);
	Shader* GetShader(char* shaderName);
};

