#include "FragmentShader.h"


FragmentShader::FragmentShader(ID3D11Device *_device, ID3DBlob *buffer)
{
	HRESULT result = _device->CreatePixelShader(buffer->GetBufferPointer(), buffer->GetBufferSize(), 0, &shader);

	if (FAILED(result))
	{
		return;
	}
}


FragmentShader::~FragmentShader()
{
}

ID3D11PixelShader* FragmentShader::GetShader()
{
	return shader;
}
