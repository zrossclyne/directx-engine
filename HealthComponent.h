#pragma once
#include "IComponent.h"

class HealthComponent
{
private:
	float health;
public:
	HealthComponent();
	~HealthComponent();

	void update();

	float GetHealth();
	void SetHealth(float health);
};

