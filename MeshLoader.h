#pragma once
#include "Globals.h"
#include <map>
#include <vector>
#include "Mesh.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Vertex.h"

using namespace Global::TypeDefinitions; 

class MeshLoader
{
public:
	MeshLoader();
	~MeshLoader();
	
	Mesh Load(char* filePath);

private:
	float *vertices, *normals, *texCoords;
	Vertex3 *sVertices;
	WORD *sIndices;

	int curAddIndex;

	WORD AddVertex(Vertex3 vertex);

	std::vector<Vertex3>      m_Vertex;
	std::vector<WORD>        m_Index;

	std::vector <DirectX::XMFLOAT3> Position;
	std::vector <DirectX::XMFLOAT2> TexCoord;
	std::vector <DirectX::XMFLOAT3> Normal;
};

