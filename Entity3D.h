#pragma once
#include "Entity.h"
#include "I3DRenderableComponent.h"

class Entity3D : public Entity, public I3DRenderableComponent
{
public:
	Entity3D();
	~Entity3D();
};

