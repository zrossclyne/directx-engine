#include "Texture.h"


Texture::Texture()
{
	
}


Texture::~Texture()
{
}

bool Texture::BuildTexture(ID3D11Device* dev)
{
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.ArraySize = 1;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	texDesc.MipLevels = 1;
	texDesc.MiscFlags = 0;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA texSub;

	texSub.pSysMem = data;
	texSub.SysMemPitch = (UINT)(width * 4);
	texSub.SysMemSlicePitch = (UINT)(width * height * 4);
	 
	HRESULT result = dev->CreateTexture2D(&texDesc, &texSub, &tex);

	return true;
}

bool Texture::LoadTexture(LPCSTR fileLocation)
{
	std::fstream ss(fileLocation, std::ios::binary | std::ios::in);

	// Move to end of file
	ss.seekg(0, std::ios::end);

	// get offset position
	int length = ss.tellg();
	
	// move back to start
	ss.seekg(0, std::ios::beg);

	byte* s = new byte[length];

	ss.read((char *)s, length);

	byte idLength = s[0];

	width = s[12] | (s[13] << 8);
	height = s[14] | (s[15] << 8);

	byte bitsPerPixel = s[16];

	if (bitsPerPixel == 32)
	{
		int startOffset = 18 + idLength;
		int readLength = length - startOffset;

		data = new byte[readLength];
		
		memcpy(data, &s[startOffset], readLength);

		return true;
	}

	delete[] s;
	s = NULL;

	return false;
}

byte* Texture::GetData()
{
	return data;
}

ID3D11Texture2D* Texture::GetTexture()
{
	return tex;
}


int Texture::GetWidth()
{
	return width;
}
int Texture::GetHeight()
{
	return height;
}