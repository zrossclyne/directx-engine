#pragma once
#include "Globals.h"

class Vertex3
{
public:
	Vertex3();
	Vertex3(float pX, float pY, float pZ, float tX, float tY);
	~Vertex3();

	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT2 texture;
	DirectX::XMFLOAT3 normal;

	bool operator==(Vertex3 b)
	{
		bool pos = this->position.x == b.position.x && this->position.y == b.position.y && this->position.z == b.position.z;
		bool tex = this->texture.x == b.texture.x & this->texture.y == b.texture.y;
		//bool norm = this->norm.x == b.norm.x && this->norm.y == b.norm.y && this->norm.z == b.norm.z;

		return pos &tex;// && norm;
	}
	bool operator==(Vertex3* b)
	{
		bool pos = this->position.x == b->position.x && this->position.y == b->position.y && this->position.z == b->position.z;
		//bool tex = this->texture.x == b->texture.x & this->texture.y == b->texture.y;
		//bool norm = this->norm.x == b->norm.x && this->norm.y == b->norm.y && this->norm.z == b->norm.z;

		return pos;// &tex;// && norm;
	}
	bool operator!=(Vertex3 b)
	{
		bool pos = this->position.x != b.position.x && this->position.y != b.position.y && this->position.z != b.position.z;
		//bool tex = this->texture.x != b.texture.x & this->texture.y != b.texture.y;
		//bool norm = this->norm.x != b.norm.x && this->norm.y != b.norm.y && this->norm.z != b.norm.z;

		return pos;// &tex;// && norm;
	}
private:
};

