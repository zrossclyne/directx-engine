#pragma once
#include <Windows.h>
#include <d3d11_1.h>
#include <D3DX11.h>
#include <DirectXMath.h>
#include "FragmentShader.h"
#include <stdexcept>

#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define RAISE __FILE__ "("__STR1__(__LINE__)") : Warning Msg: "

namespace Global
{
	namespace TypeDefinitions
	{
		typedef unsigned short ushort;
		typedef unsigned int uint;
	}

	namespace Structures
	{
		struct Vertex
		{
			DirectX::XMFLOAT3 pos;
			DirectX::XMFLOAT2 tex0;

			DirectX::XMFLOAT3 norm;

			bool operator==(Vertex b)
			{
				bool pos = this->pos.x == b.pos.x && this->pos.y == b.pos.y && this->pos.z == b.pos.z;
				bool tex = this->tex0.x == b.tex0.x && this->tex0.y == b.tex0.y;
				bool norm = this->norm.x == b.norm.x && this->norm.y == b.norm.y && this->norm.z == b.norm.z;

				return pos && tex && norm;
			}
		};
	}

	namespace Enums
	{
		enum GameState
		{
			LOADING,
			RUNNING,
			PAUSED,
			QUIT
		};

		enum ShaderType
		{
			VERTEX_SHADER = 0,
			TESSELLATION_CONTROL_SHADER = 1,
			TESSELLATION_EVALUATION_SHADER = 2,
			GEOMETRY_SHADER = 3,
			PIXEL_SHADER = 4,
			FRAGMENT_SHADER = 4
		};
	}
}