#pragma once
#include "Globals.h"
#include "Mesh.h"
#include "Instance.h"
#include "IRenderableComponent.h"

using namespace Global::Enums;

class Entity : public IRenderableComponent
{
private:
	char* shaders[5];
	char* texture;
	Instance instanceData;

	bool collidable;

	AxisAlignedBox boundingBox;

	IRenderableComponent::mesh;
public:
	Entity();
	Entity(Mesh* mesh, char* vertexShader, char* fragmentShader);
	void AssignShader(ShaderType type, char* shaderName);
	void AssignTexture(char* texture);
	~Entity();

	std::vector<IComponent*> components;

	bool visible;

	char** GetShaders();
	char* GetShader(ShaderType type);
	char* GetTexture();
	Mesh* GetMesh();

	DirectX::XMFLOAT3 GetPosition();
	DirectX::XMFLOAT3 GetRotation();
	DirectX::XMFLOAT3 GetScale();

	void RotateBy(float x, float y, float z);

	void SetPosition(DirectX::XMFLOAT3 position);
	void SetScale(DirectX::XMFLOAT3 position);
	void SetRotation(DirectX::XMFLOAT3 position);
	void SetTexture(int texIndex);
	
	AxisAlignedBox* GetBoundingBox();

	void SetCollidable(bool collision);

	void MoveX(float val);
	void MoveY(float val);
	void MoveZ(float val);

	friend bool operator==(const Entity &left, const Entity &right)
	{
		if (left.mesh == right.mesh && left.instanceData == right.instanceData)
		{
			return true;
		}

		return false;
	}
	friend bool operator!=(const Entity &left, const Entity &right)
	{
		return !(left == right);
	}

	void update();
	void render();

	Instance* GetInstanceData();


	friend bool operator^(Entity &entA, Entity &entB)
	{
		return Collision::CheckCollision(entA.GetBoundingBox(), entB.GetBoundingBox());
	}

	std::vector<IComponent*>* GetComponents();
	void AddComponent(IComponent* component);

};

