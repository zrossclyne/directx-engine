#include "ShaderManager.h"


ShaderManager::ShaderManager(ID3D11Device *_device)
{
	this->_device = _device;
}


ShaderManager::~ShaderManager()
{
}

void ShaderManager::AddShader(char* shaderName, ShaderType shaderType, char* shaderPath, char* entryPoint, char* shaderModel)
{
	if (shaderType == VERTEX_SHADER)
	{
		std::_Debug_message(L"Vertex Shader must be created with a D3D11_INPUT_ELEMENT_DESC. Please use overloaded function", L"AddShader(char*, ShaderType, char*, char*, char*)", NULL);
	}

	this->AddShader(shaderName, shaderType, shaderPath, entryPoint, shaderModel, NULL);
}

void ShaderManager::AddShader(char* shaderName, ShaderType shaderType, char* shaderPath, char* entryPoint, char* shaderModel, std::vector<D3D11_INPUT_ELEMENT_DESC>* shaderInterface)
{
	DWORD shaderFlags = (1 << 11);
	ID3DBlob* buffer;
	ID3DBlob* errorBuffer;

	HRESULT result = D3DX11CompileFromFile(shaderPath, 0, 0, entryPoint, shaderModel, shaderFlags, 0, 0, &buffer, &errorBuffer, 0);

	if (errorBuffer != 0)
	{
		errorBuffer->Release();
	}

	if (FAILED(result))
	{
		return;
	}

	Shader* shader;

	switch (shaderType)
	{
	case VERTEX_SHADER:
		shader = new VertexShader(_device, buffer, shaderInterface);
		break;
	case PIXEL_SHADER | FRAGMENT_SHADER:
		shader = new FragmentShader(_device, buffer);

		break;
	}

	shaders.insert(std::make_pair(shaderName, std::make_pair(shaderType, shader)));

}

Shader* ShaderManager::GetShader(char* shaderName)
{
	return shaders[shaderName].second;
}