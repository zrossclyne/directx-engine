#include "EntityManager.h"


EntityManager::EntityManager()
{
}


EntityManager::~EntityManager()
{
}

void EntityManager::AddEntity(char* name, Mesh* mesh, char* vertexShader, char* fragmentShader)
{
	Entity e = Entity(mesh, vertexShader, fragmentShader);

	//entities.insert(std::make_pair(name, e));
	entities.insert(std::make_pair(name, e));
}

void EntityManager::RemoveEntity(Entity* name)
{
	auto iter = entities.begin();

	bool found = false;
	
	for (auto it = entities.begin(); it != entities.end(); it++)
	{
		if (it->second == *name)
		{
			found = true;
			iter = it;
		}
	}

	if (found)
	{
		entities.erase(iter);
	}
}

Entity* EntityManager::GetEntity(char* name)
{
	return &entities[name];
}


std::map<char*, Entity>* EntityManager::GetEntities()
{
	return &entities;
}