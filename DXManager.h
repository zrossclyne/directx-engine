#pragma once
#include "Globals.h"
#include "Vertex.h"
#include "Instance.h"

#include "Camera.h"

#include "TextureManager.h"
#include "ShaderManager.h"
#include "EntityManager.h"
#include "MeshManager.h"

class DXManager
{
public:
	DXManager(HINSTANCE hInstance, HWND hwnd);
	~DXManager();

	bool Init(HINSTANCE hInstance, HWND hwnd);

	EntityManager* GetEntityManager();
	TextureManager* GetTextureManager();
	ShaderManager* GetShaderManager();
	MeshManager* GetMeshManager();

	bool CreateShaderInterface();

	void DrawInstanced();

	Camera* GetCamera();
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();

	void ToggleFullscreen();
	void DisableFullscreen();

	void(*OnRender) ();

private:
	// Managers
	EntityManager* entManager;
	TextureManager* txtManager;
	ShaderManager* shdManager;
	MeshManager* meshManager;

	bool fullscreen;

	Camera camera;

	std::vector<Instance> instances;

	void CreateInstanceBuffer(std::vector<Instance*>* instanceData, ID3D11Buffer** buffer);

	void Render(Mesh* mesh, ID3D11Buffer* instanceBuffer, int instanceCount);
	bool ValidateShader(char** shader);

	// Abstract DX Classes
	ID3D11Device *_device;
	ID3D11DeviceContext *_deviceContext;
	ID3D11RenderTargetView *_backBufferTarget;
	ID3D11DepthStencilView *_depthStencilView;
	ID3D11DepthStencilState *_depthStencilState;
	ID3D11Texture2D *_depthTexture;
	ID3D11RasterizerState *_rasterState;

	ID3D11Buffer* viewProjectionCB;
	ID3D11Buffer* cameraPosCB;
	ID3D11Buffer* timeCB;

	IDXGISwapChain *_swapChain;
	
	D3D_FEATURE_LEVEL _featureLevel;
	D3D_DRIVER_TYPE _driverType;

	HINSTANCE _hInstance;
	HWND _hwnd;

	ushort winHeight;
	ushort winWidth;

	bool InitDeviceAndSwapChain();
	bool InitRenderTargetView();
	bool InitTexture2D();
	bool InitDepthStencil();
	bool InitRasterizer();
	bool InitViewport();
	bool InitViewProjection();

	void PrepareInstances(Mesh* mesh);

	void CreateInstances(std::vector<std::vector<Instance>>* instances);

	DirectX::XMMATRIX GetViewProjection();
};

