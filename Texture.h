#pragma once
#include "Globals.h"

#include <fstream>

class Texture
{
private:
	int width;
	int height;

	byte* data;

	ID3D11Texture2D* tex;
public:
	Texture();
	~Texture();

	bool LoadTexture(LPCSTR fileLocation);
	bool BuildTexture(ID3D11Device* dev);
	ID3D11Texture2D* GetTexture();

	int GetWidth();
	int GetHeight();

	byte* GetData();
};

