#include "MeshLoader.h"

std::vector<std::string> split(const std::string &str, char delimiter)
{
	std::vector<std::string> rtn;

	std::string::size_type i = 0;
	std::string::size_type j = str.find(delimiter);

	while (j != std::string::npos)
	{
		rtn.push_back(str.substr(i, j - i));
		i = ++j;
		j = str.find(delimiter, j);

		if (j == std::string::npos)
		{
			rtn.push_back(str.substr(i, str.length()));
		}
	}

	return rtn;
}


MeshLoader::MeshLoader() : vertices(0)
{
	curAddIndex = -1;
}


MeshLoader::~MeshLoader()
{
}

Mesh MeshLoader::Load(char* fileName)
{
	std::fstream f(fileName, std::ios_base::in | std::ios::binary);
	if (f) //File is ready for reading
	{
		short vertexStride = sizeof(Vertex3);					//Gets the stride per vertex
		long fileLength = f.seekg(0, std::ios::end).tellg();	//Gets the byte count
		float vertexCount = (float)fileLength / (float)vertexStride;			//Calculates the vertex count

		if (vertexCount == (int)vertexCount)					//Ensures there are no incomplete vertices
		{
			if (vertexCount > 134217727 || vertexCount * vertexStride > 2147483647)
			{
				//Alloc will definitely fail.
				//Throw(ERROR_MEMORY_ALLOCATION_ASSERT);
			}
			else
			{
				char* vertices = new __nothrow char[fileLength];
				if (vertices == NULL)
				{
					//Allocation failed
					//Throw(ERROR_MEMORY_ALLOCATION_FAILED);
				}
				else
				{
					//Read all vertices directly into the array and build the buffer
					if (f.seekg(0, std::ios::beg))
					{
						f.read(vertices, fileLength);
						for (int i = 0; i < fileLength; i++)
						{
							//1.6345 -0.0305 0.4379
							char b = vertices[i];
							if (i == fileLength - 32)
								int i2 = 12;
						}
						Mesh m;

						m.Build((Vertex3*)vertices, (int)vertexCount);

						return m;

						//std::vector<Vertex> debugData;
						//Vertex* iterator = reinterpret_cast<Vertex*>(vertices);
						//for (int i = 0; i < vertexCount; i++)
						//{
						//	debugData.push_back(iterator[i]);
						//}
					}
					else
					{
						//Throw(ERROR_IO_SEEK_FAIL);
					}
				}
				delete[] vertices;
				vertices = NULL;
			}
		}
		else
		{
			//Throw(ERROR_MESH_CORRUPT);
		}
	}
	else
	{
		//Throw(ERROR_IO_FILE_INVALID);
	}

	return *reinterpret_cast<Mesh*>(NULL);
}