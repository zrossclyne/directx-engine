#include "DXManager.h"


DXManager::DXManager(HINSTANCE hInstance, HWND hwnd)
{
	Init(hInstance, hwnd);

	RECT windowRect;

	GetClientRect(hwnd, &windowRect);

	winHeight = abs(windowRect.top - windowRect.bottom);
	winWidth = abs(windowRect.right - windowRect.left);

	txtManager = new TextureManager(_device);
	shdManager = new ShaderManager(_device);
	entManager = new EntityManager();
	meshManager = new MeshManager();

	camera = Camera();
}

Camera* DXManager::GetCamera()
{
	return &camera;
}

bool DXManager::InitDeviceAndSwapChain()
{
	RECT dimensions;
	GetClientRect(_hwnd, &dimensions);

	unsigned int width = dimensions.right - dimensions.left;
	unsigned int height = dimensions.bottom - dimensions.top;

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_SOFTWARE
	};

	unsigned int totalDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0
	};

	unsigned int totalFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferDesc.Height = height;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = _hwnd;
	swapChainDesc.Windowed = !fullscreen;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	unsigned int creationFlags = 0;

	HRESULT result;
	unsigned int driver = 0;

	for (driver = 0; driver < totalDriverTypes; ++driver)
	{
		result = D3D11CreateDeviceAndSwapChain(0, driverTypes[driver], 0, D3D11_CREATE_DEVICE_DEBUG, featureLevels, totalFeatureLevels, D3D11_SDK_VERSION, &swapChainDesc, &_swapChain, &_device, &_featureLevel, &_deviceContext);

		if (SUCCEEDED(result))
		{
			_driverType = driverTypes[driver];
			break;
		}
	}

	if (FAILED(result))
	{
		return false;
	}

	return true;
}

void DXManager::ToggleFullscreen()
{
	fullscreen = !fullscreen;

	_swapChain->SetFullscreenState(fullscreen, NULL);
}

void DXManager::DisableFullscreen()
{
	if (fullscreen)
	{
		fullscreen = false;

		_swapChain->SetFullscreenState(fullscreen, NULL);
	}
}

bool DXManager::InitRenderTargetView()
{
	ID3D11Texture2D *backBufferTexture;

	HRESULT result = _swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&backBufferTexture);

	if (FAILED(result))
	{
		return false;
	}

	result = _device->CreateRenderTargetView(backBufferTexture, 0, &_backBufferTarget);

	if (backBufferTexture)
	{
		backBufferTexture->Release();
	}

	if (FAILED(result))
	{
		return false;
	}

	return true;
}

bool DXManager::InitTexture2D()
{
	RECT dimensions;
	GetClientRect(_hwnd, &dimensions);

	unsigned int width = dimensions.right - dimensions.left;
	unsigned int height = dimensions.bottom - dimensions.top;

	D3D11_TEXTURE2D_DESC depthTexDesc;
	ZeroMemory(&depthTexDesc, sizeof(depthTexDesc));
	depthTexDesc.Width = width;
	depthTexDesc.Height = height;
	depthTexDesc.MipLevels = 1;
	depthTexDesc.ArraySize = 1;
	depthTexDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthTexDesc.SampleDesc.Count = 1;
	depthTexDesc.SampleDesc.Quality = 0;
	depthTexDesc.Usage = D3D11_USAGE_DEFAULT;
	depthTexDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthTexDesc.CPUAccessFlags = 0;
	depthTexDesc.MiscFlags = 0;

	HRESULT result = _device->CreateTexture2D(&depthTexDesc, NULL, &_depthTexture);

	if (FAILED(result))
	{
		return false;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));

	descDSV.Format = depthTexDesc.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	result = _device->CreateDepthStencilView(_depthTexture, &descDSV, &_depthStencilView);

	if (FAILED(result))
	{
		return false;
	}

	_deviceContext->OMSetRenderTargets(1, &_backBufferTarget, _depthStencilView);

	return true;
}

bool DXManager::InitDepthStencil()
{
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	ZeroMemory(&dsDesc, sizeof(dsDesc));

	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	HRESULT result = _device->CreateDepthStencilState(&dsDesc, &_depthStencilState);

	if (FAILED(result))
	{
		return false;
	}

	_deviceContext->OMSetDepthStencilState(_depthStencilState, 1);

	return true;
}

bool DXManager::InitRasterizer()
{
	D3D11_RASTERIZER_DESC rasterDesc;
	ZeroMemory(&rasterDesc, sizeof(rasterDesc));

	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = /**/D3D11_FILL_SOLID/*/D3D11_FILL_WIREFRAME/**/;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	HRESULT result = _device->CreateRasterizerState(&rasterDesc, &_rasterState);

	if (FAILED(result))
	{
		return false;
	}

	_deviceContext->RSSetState(_rasterState);

}

bool DXManager::InitViewport()
{
	RECT dimensions;
	GetClientRect(_hwnd, &dimensions);

	unsigned int width = dimensions.right - dimensions.left;
	unsigned int height = dimensions.bottom - dimensions.top;

	D3D11_VIEWPORT viewport;

	viewport.Width = static_cast<float>(width);
	viewport.Height = static_cast<float>(height);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	_deviceContext->RSSetViewports(1, &viewport);

	return true;
}

bool DXManager::InitViewProjection()
{
	D3D11_BUFFER_DESC constDesc;
	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = sizeof(DirectX::XMMATRIX);
	constDesc.Usage = D3D11_USAGE_DEFAULT;
	HRESULT result = _device->CreateBuffer(&constDesc, 0, &viewProjectionCB);

	if (FAILED(result))
	{
		return false;
	}

	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = sizeof(DirectX::XMMATRIX);
	constDesc.Usage = D3D11_USAGE_DEFAULT;
	result = _device->CreateBuffer(&constDesc, 0, &timeCB);

	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = sizeof(DirectX::XMMATRIX);
	constDesc.Usage = D3D11_USAGE_DEFAULT;
	result = _device->CreateBuffer(&constDesc, 0, &cameraPosCB);

	return true;
}

bool DXManager::Init(HINSTANCE hInstance, HWND hwnd)
{
	_hInstance = hInstance;
	_hwnd = hwnd;

	fullscreen = false;

	return InitDeviceAndSwapChain() && InitRenderTargetView() && InitTexture2D() && InitDepthStencil() && InitRasterizer() && InitViewport() && InitViewProjection();
}

DXManager::~DXManager()
{
}

bool DXManager::CreateShaderInterface()
{
	return true;
}

EntityManager* DXManager::GetEntityManager()
{
	return entManager;
}

TextureManager* DXManager::GetTextureManager()
{

	return txtManager;
}

ShaderManager* DXManager::GetShaderManager()
{
	return shdManager;
}

MeshManager* DXManager::GetMeshManager()
{
	return meshManager;
}

int offset = 0;
int hello = 0;
void DXManager::DrawInstanced()
{
	float clearColor[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
	_deviceContext->ClearRenderTargetView(_backBufferTarget, clearColor);
	_deviceContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	if (OnRender != NULL)
	{
		OnRender();
	}
	
	typedef std::multimap<char**, Instance*> ObjInstance;
	typedef std::map<Mesh*, ObjInstance> EntityInstance;

	EntityInstance instances;

	for (auto ent = entManager->GetEntities()->begin(); ent != entManager->GetEntities()->end(); ent++)
	{
		if (ent->second.visible)
		{
			instances[ent->second.GetMesh()].insert(std::make_pair(ent->second.GetShaders(), ent->second.GetInstanceData()));
		}
	}

	
	std::vector<Instance*> instanceData;

	for (auto instArr = instances.begin(); instArr != instances.end(); instArr++)
	{
		// each EntityInstance element
		char** prevShader = new char*[5] { "", "", "", "", "" };

		instanceData.clear();

		ID3D11Buffer* curBuffer;

		for (auto inst = instArr->second.begin(); inst != instArr->second.end(); inst++)
		{
			// each ObjInstance element
			if (*prevShader != *inst->first)
			{
				delete[] prevShader;
				prevShader = inst->first;
			}
			
			instanceData.push_back(inst->second);
		}

		if (ValidateShader(prevShader))
		{
			// Draw in here
			VertexShader* vShader = ((VertexShader *)shdManager->GetShader(prevShader[VERTEX_SHADER]));
			FragmentShader* fShader = (FragmentShader *)shdManager->GetShader(prevShader[FRAGMENT_SHADER]);
			
			ID3D11PixelShader* pixelShader = fShader->GetShader();

			ID3D11VertexShader* vertexShader = vShader->GetShader();
			ID3D11InputLayout* inputLayout = vShader->GetInputLayout();

			_deviceContext->VSSetShader(vertexShader, 0, 0);
			_deviceContext->PSSetShader(pixelShader, 0, 0);

			_deviceContext->IASetInputLayout(inputLayout);

			CreateInstanceBuffer(&instanceData, &curBuffer);
			Render(instArr->first, curBuffer, instanceData.size());
		}
	}

	instanceData.clear();
	instances.clear();

	_swapChain->Present(0, 0);
}

bool DXManager::ValidateShader(char** shader)
{
	bool valid = false;

	for (int i = 0; i < 5; i++)
	{
		if (shader[i] != "")
			valid = true;
	}

	return valid;
}

void DXManager::CreateInstanceBuffer(std::vector<Instance*>* instanceData, ID3D11Buffer** buffer)
{
	std::vector<Instance> locInstances;

	for (int i = 0; i < instanceData->size(); i++)
	{
		locInstances.push_back(*instanceData->at(i));
	}

	D3D11_BUFFER_DESC instDesc;
	ZeroMemory(&instDesc, sizeof(instDesc));

	instDesc.Usage = D3D11_USAGE_DEFAULT;
	instDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instDesc.ByteWidth = sizeof(Instance)* instanceData->size();
	instDesc.StructureByteStride = sizeof(Instance)* instanceData->size();
	instDesc.CPUAccessFlags = 0;
	instDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));
	resourceData.pSysMem = &locInstances[0];
	
	HRESULT result = _device->CreateBuffer(&instDesc, &resourceData, buffer);

	locInstances.clear();
}

void DXManager::PrepareInstances(Mesh* mesh)
{
	instances.clear();

}

void DXManager::Render(Mesh* mesh, ID3D11Buffer* instanceBuffer, int instanceCount)
{
	HRESULT result;

	DirectX::XMMATRIX pMatrix;
	DirectX::XMMATRIX vMatrix;
	DirectX::XMMATRIX wMatrix;
	ID3D11Buffer *vBuffer;
	ID3D11Buffer *pBuffer;
	ID3D11Buffer *wBuffer;

	ID3D11VertexShader *vertexShader;
	ID3D11PixelShader *pixelShader;

	DirectX::XMMATRIX tmpMatrix = DirectX::XMMatrixTranslation(0, 0, 10);
	vMatrix = DirectX::XMMatrixTranspose(tmpMatrix);

	RECT dimensions;
	GetClientRect(_hwnd, &dimensions);

	winWidth = dimensions.right - dimensions.left;
	winHeight = dimensions.bottom - dimensions.top;

	tmpMatrix = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PIDIV4, ((float)winWidth / (float)winHeight), 0.01f, 1500.0f);
	pMatrix = XMMatrixTranspose(tmpMatrix);

	//ID3D11ShaderResourceView** _colorMap = txtManager->GetTexture("Texture");
	
	ID3D11InputLayout *inputLayout;

	if (_deviceContext == 0)
	{
		return;
	}

	Vertex3* vertices = new Vertex3[mesh->GetVertices().size()];
	WORD* indices = new WORD[mesh->GetIndices().size()];

	std::vector<Vertex3> verts = mesh->GetVertices();
	std::vector<WORD> inds = mesh->GetIndices();

	for (int i = 0; i < verts.size(); i++)
	{
		vertices[i] = verts.at(i);
	};
	for (int i = 0; i < inds.size(); i++)
	{
		indices[i] = inds.at(i);
	}

	ID3D11Buffer *vertexBuffer;
	ID3D11Buffer *indexBuffer;

	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));
	vertexDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeof(Vertex3)* mesh->GetVertices().size();

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));
	resourceData.pSysMem = &vertices[0];

	result = _device->CreateBuffer(&vertexDesc, &resourceData, &vertexBuffer);

	if (FAILED(result))
	{
		bool breakpointHere = true;
	}

	D3D11_BUFFER_DESC indexDesc;

	ZeroMemory(&indexDesc, sizeof(indexDesc));
	indexDesc.Usage = D3D11_USAGE_DEFAULT;
	indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexDesc.ByteWidth = sizeof(WORD)* mesh->GetIndices().size();
	indexDesc.CPUAccessFlags = 0;

	resourceData.pSysMem = &indices[0];

	result = _device->CreateBuffer(&indexDesc, &resourceData, &indexBuffer);

	tmpMatrix = DirectX::XMMatrixIdentity();
	wMatrix = XMMatrixTranspose(tmpMatrix);

	ID3D11Buffer *renderBuffer[2] = { vertexBuffer, instanceBuffer };
	UINT stride[2] = { sizeof(Vertex3), sizeof(Instance) };
	UINT offset[2] = { 0, 0 };

	// start render sequence after this

	// render stuff here
	ID3D11RasterizerState *rastState;
	D3D11_RASTERIZER_DESC rastDesc;

	_deviceContext->RSGetState(&rastState);
	rastState->GetDesc(&rastDesc);

	rastDesc.FillMode = D3D11_FILL_SOLID;

	_device->CreateRasterizerState(&rastDesc, &rastState);

	_deviceContext->RSSetState(rastState);

	_deviceContext->IASetVertexBuffers(0, 2, renderBuffer, stride, offset);
	//_deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, stride, offset);
	_deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
	_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	_deviceContext->PSSetShaderResources(0, 1, txtManager->GenerateShaderResource());
	//_deviceContext->PSSetShaderResources(0, 1, _colorMap);
	_deviceContext->PSSetSamplers(0, 1, &txtManager->TextureSampler);

	DirectX::XMMATRIX viewMat = XMMatrixMultiply(camera.GetViewProjection(), XMMatrixTranspose(pMatrix));// XMMatrixMultiply(XMMatrixIdentity(), pMatrix);

	viewMat = DirectX::XMMatrixTranspose(viewMat);

	FLOAT time = GetTickCount();


	_deviceContext->UpdateSubresource(viewProjectionCB, 0, 0, &viewMat, 0, 0);
	_deviceContext->UpdateSubresource(cameraPosCB, 0, 0, &(camera.GetLookDirection()), 0, 0);
	_deviceContext->UpdateSubresource(timeCB, 0, 0, &time, 0, 0);
	
	_deviceContext->VSSetConstantBuffers(0, 1, &viewProjectionCB);
	_deviceContext->VSSetConstantBuffers(1, 1, &timeCB);

	_deviceContext->PSSetConstantBuffers(0, 1, &cameraPosCB);

	/**/
	//_deviceContext->DrawInstanced(mesh->GetVertices().size(), instanceCount, 0, 0);
	_deviceContext->DrawIndexedInstanced(mesh->GetIndices().size(), instanceCount, 0, 0, 0);
	/*/
	_deviceContext->DrawIndexed(36, 0, 0);
	/**/

	delete[] vertices;
	delete[] indices;

	vertices = NULL;
	indices = NULL;
	
	rastState->Release();

	vertexBuffer->Release();
	
	indexBuffer->Release();
	
	instanceBuffer->Release();
	//delete[] renderBuffer;
}

ID3D11Device* DXManager::GetDevice()
{
	return _device;
}

ID3D11DeviceContext* DXManager::GetDeviceContext()
{
	return _deviceContext;
}