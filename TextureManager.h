#pragma once
#include "Globals.h"
#include "Texture.h"
#include <vector>

using namespace Global::TypeDefinitions;

class TextureManager
{
private:
	ID3D11Device *_device;
	
	ID3D11ShaderResourceView* shaderResource;
	ID3D11SamplerState* samplerState;

	std::vector<std::pair<char*, Texture>> textures;

	bool valid;
public:
	ID3D11SamplerState *TextureSampler;

	TextureManager(ID3D11Device* _device);
	~TextureManager();
	void AddTexture(char* shaderName, char* filePath);

	ID3D11ShaderResourceView** GenerateShaderResource();

	int GetTextureIndex(char* tag);

	bool IsValid();
};

