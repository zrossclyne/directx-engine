Texture2DArray _colorMap : register(t0);
SamplerState _colorMapSampler : register(s0);

cbuffer cbCameraPosition : register(b0)
{
	float3 cameraPosition;
}

struct PS_Input
{
	float4 pos : SV_POSITION;
	float3 norm : NORMAL;
	float3 tex0 : TEXCOORD0;
};

float4 PS_Main(PS_Input frag) : SV_TARGET
{
	float4 var = _colorMap.Sample(_colorMapSampler, frag.tex0);

	float dotProd = dot(frag.norm, cameraPosition);
	float val = 0.0f;

	if (dotProd < 0.1f)
	{
		val = 0.0f;
	}
	else if (dotProd < 0.25f)
	{
		val = 0.25f;
	}
	else if (dotProd < 0.5f)
	{
		val = 0.5f;
	}
	else if (dotProd < 0.75f)
	{
		val = 0.75f;
	}
	else if (dotProd < 1.0f)
	{
		val = 1.0f;
	}

	//return float4(frag.norm, 1);

	return mul(var, val);
}