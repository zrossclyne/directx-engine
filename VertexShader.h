#pragma once
#include "Globals.h"
#include <vector>

class VertexShader : public Shader
{
private:
	ID3D11VertexShader *shader;
	ID3D11InputLayout *inputLayout;
public:
	VertexShader(ID3D11Device *_device, ID3DBlob *buffer, std::vector<D3D11_INPUT_ELEMENT_DESC>* shaderInterface);
	~VertexShader();

	ID3D11VertexShader* GetShader();
	ID3D11InputLayout* GetInputLayout();
};

