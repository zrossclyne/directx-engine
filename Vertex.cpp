#include "Vertex.h"


Vertex3::Vertex3()
{
	position = DirectX::XMFLOAT3(0, 0, 0);
	texture = DirectX::XMFLOAT2(0, 0);
	normal = DirectX::XMFLOAT3(0, 0, 0);
}

Vertex3::Vertex3(float pX, float pY, float pZ, float tX, float tY)
{
	position.x = pX;
	position.y = pY;
	position.z = pZ;

	texture.x = tX;
	texture.y = tY;

	normal = { 0, 0, 0 };
}


Vertex3::~Vertex3()
{
}
