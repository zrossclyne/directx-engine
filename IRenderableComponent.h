#pragma once
#include "IComponent.h"
#include "Mesh.h"

class IRenderableComponent : public IComponent
{
private:

public:
	IRenderableComponent(Mesh* mesh);
	IRenderableComponent();
	~IRenderableComponent();

	Mesh* mesh;

	virtual void update() = 0;
	virtual void render() = 0;
};

