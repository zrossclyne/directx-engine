#pragma once
class IComponent
{
public:
	IComponent();
	~IComponent();

	virtual void update() = 0;
};

