#include "TextureManager.h"


TextureManager::TextureManager(ID3D11Device* _device)
{
	this->_device = _device;

	D3D11_SAMPLER_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	textureDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	textureDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	textureDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	textureDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	textureDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HRESULT result = _device->CreateSamplerState(&textureDesc,
		&TextureSampler);

	ID3D11DeviceContext* devCon;
	_device->GetImmediateContext(&devCon);

	devCon->PSSetSamplers(0, 1, &TextureSampler);

	devCon = NULL;
}


TextureManager::~TextureManager()
{
}

void TextureManager::AddTexture(char* textureName, char* filePath)
{
	Texture t = Texture();
	t.LoadTexture(filePath);
	
	valid = false;

	textures.push_back(std::make_pair(textureName, t));
}

ID3D11ShaderResourceView** TextureManager::GenerateShaderResource()
{
	if (!valid)
	{
		int width, height;
		width = textures[0].second.GetWidth();
		height = textures[0].second.GetHeight();

		D3D11_TEXTURE2D_DESC texDesc;
		texDesc.Width = width;
		texDesc.Height = height;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = textures.size();
		texDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texDesc.CPUAccessFlags = 0;
		texDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA* texData = new D3D11_SUBRESOURCE_DATA[textures.size()];

		for (int i = 0; i < textures.size(); i++)
		{
			Texture* t = &(textures[i].second);

			texData[i].pSysMem = t->GetData();
			texData[i].SysMemPitch = (UINT)(width * 4);
			texData[i].SysMemSlicePitch = (UINT)(width * height * 4);
		}

		ID3D11Texture2D* resource;

		_device->CreateTexture2D(&texDesc, texData, &resource);

		delete[] texData;

		D3D11_SHADER_RESOURCE_VIEW_DESC desc;

		desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
		desc.Texture2DArray.ArraySize = textures.size();
		desc.Texture2DArray.FirstArraySlice = 0;
		desc.Texture2DArray.MipLevels = 1;
		desc.Texture2DArray.MostDetailedMip = 0;

		ID3D11ShaderResourceView* tmpResource = NULL;

		HRESULT res = _device->CreateShaderResourceView(resource, &desc, &tmpResource);

		if (FAILED(res))
		{
			return NULL;
		}
		else
		{
			valid = true;

			shaderResource = tmpResource;
		}
	}

	return &shaderResource;
}

bool TextureManager::IsValid()
{
	return valid;
}

int TextureManager::GetTextureIndex(char* tag)
{
	for (int i = 0; i < textures.size(); i++)
	{
		if (textures[i].first == tag)
		{
			return i;
		}
	}

	return -1;
}