#include "Collision.h"


Collision::Collision()
{
}


Collision::~Collision()
{
}

static inline BOOL XMVector3AnyTrue(DirectX::FXMVECTOR V)
{
	DirectX::XMVECTOR C;

	// Duplicate the fourth element from the first element.
	C = DirectX::XMVectorSwizzle(V, 0, 1, 2, 0);

	return XMComparisonAnyTrue(DirectX::XMVector4EqualIntR(C, DirectX::XMVectorTrueInt()));
}

bool Collision::CheckCollision(AxisAlignedBox* volumeA, AxisAlignedBox* volumeB)
{
	using namespace DirectX;

	DirectX::XMVECTOR CenterA = XMLoadFloat3(&volumeA->Center);
	DirectX::XMVECTOR ExtentsA = XMLoadFloat3(&volumeA->Extents);

	DirectX::XMVECTOR CenterB = XMLoadFloat3(&volumeB->Center);
	DirectX::XMVECTOR ExtentsB = XMLoadFloat3(&volumeB->Extents);

	DirectX::XMVECTOR MinA = CenterA - ExtentsA;
	DirectX::XMVECTOR MaxA = CenterA + ExtentsA;

	DirectX::XMVECTOR MinB = CenterB - ExtentsB;
	DirectX::XMVECTOR MaxB = CenterB + ExtentsB;

	// for each i in (x, y, z) if a_min(i) > b_max(i) or b_min(i) > a_max(i) then return FALSE
	DirectX::XMVECTOR Disjoint = DirectX::XMVectorOrInt(DirectX::XMVectorGreater(MinA, MaxB), DirectX::XMVectorGreater(MinB, MaxA));

	return !XMVector3AnyTrue(Disjoint);
}
