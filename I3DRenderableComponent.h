#pragma once
#include "IRenderableComponent.h"

class I3DRenderableComponent : public IRenderableComponent
{
private:
public:
	I3DRenderableComponent();
	~I3DRenderableComponent();

	virtual void update() = 0;
	virtual void render() = 0;
};

