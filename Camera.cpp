#include "Camera.h"


Camera::Camera()
{
	x = y = z = 0;
	rotX = rotY = rotZ = 0;
}


Camera::~Camera()
{
}


void Camera::MoveUp(float val)
{
	y += val;
}

void Camera::MoveDown(float val)
{
	y -= val;
}

void Camera::MoveLeft(float val) 
{
	x -= val;
}

void Camera::MoveRight(float val)
{
	x += val;
}

void Camera::MoveForward(float val)
{
	z += val;
}

void Camera::MoveBackward(float val)
{
	z += val;
}

void Camera::LookAt(Vector location)
{

}

void Camera::LookAt(float x, float y, float z)
{

}

DirectX::XMFLOAT3 Camera::GetLookDirection()
{
	//0,0,1 rotated by rotX, rotY, rotZ
	float locX = 0.0f;
	float locY = 0.0f;
	float locZ = -1.0f;

	// X Rotation
	float rtnY = locY * cos(rotX) - locZ * sin(rotX);
	float rtnZ = locY * sin(rotX) + locZ * sin(rotX);

	// Y Rotation
	float rtnX = locX * cos(rotY) + locZ * sin(rotY);
	rtnZ = -locX * sin(rotY) + locZ * cos(rotY);

	// Z Rotation
	rtnX = locX * cos(rotZ) - locY * sin(rotZ);
	rtnY = locX * sin(rotZ) + locY * sin(rotZ);

	return { rtnX, rtnY, rtnZ };
}

void Camera::MoveTo(Vector location)
{
	x = location.x;
	y = location.y;
	z = location.z;
}

void Camera::MoveTo(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = y;
}

void Camera::Rotate(Vector rotation)
{
	rotX = rotation.x;
	rotY = rotation.y;
	rotZ = rotation.z;
}

void Camera::Rotate(float roll, float pitch, float yaw)
{
	rotX = roll;
	rotY = pitch;
	rotZ = yaw;
}

void Camera::RotateBy(float roll, float pitch, float yaw)
{
	rotX += roll;
	rotY += pitch;
	rotZ += yaw;
}

DirectX::XMMATRIX Camera::GetViewProjection()
{
	/**/
	DirectX::XMFLOAT3 look(0, 0, 1);
	DirectX::XMFLOAT3 position(x, y, z);
	DirectX::XMFLOAT3 up(0, 1, 0);
	DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationRollPitchYaw(rotX, rotY, rotZ);

	DirectX::XMVECTOR rotVec = DirectX::XMVector3Transform(DirectX::XMLoadFloat3(&look), rotMatrix);

	//rotate look by camera rot

	return DirectX::XMMatrixLookToLH(DirectX::XMLoadFloat3(&position), rotVec, XMLoadFloat3(&up));
	/*/
	DirectX::XMFLOAT3 look(15, 0, 15);
	DirectX::XMFLOAT3 position(30, 50, 30);
	DirectX::XMFLOAT3 up(0, 1, 0);
	DirectX::XMMATRIX rotMatrix = DirectX::XMMatrixRotationRollPitchYaw(rotX, rotY, rotZ);

	DirectX::XMVECTOR rotVec = DirectX::XMVector3Transform(DirectX::XMLoadFloat3(&look), rotMatrix);

	//rotate look by camera rot

	return DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&position), DirectX::XMLoadFloat3(&look), DirectX::XMLoadFloat3(&up));
	/**/
}

float Camera::GetDistance()
{
	return distance;
}

void Camera::SetDistance(float dist)
{
	distance = dist;
}

DirectX::XMFLOAT3 Camera::GetPosition()
{
	return DirectX::XMFLOAT3(x, y, z);
}

AxisAlignedBox* Camera::GetBoundingBox()
{
	boundingBox.Center.x = x;
	boundingBox.Center.y = y;
	boundingBox.Center.z = z;

	boundingBox.Extents.x = 1.0f;
	boundingBox.Extents.y = 0.1f;
	boundingBox.Extents.z = 1.0f;

	return &boundingBox;
}