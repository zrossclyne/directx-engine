#include "Mesh.h"

Mesh::Mesh()
{

}

Mesh::Mesh(std::vector<Vertex3> m_Vertex, std::vector<WORD> m_Index)
{
	vertices = m_Vertex;
	indices = m_Index;
}

void Mesh::Build(Vertex3* vertices, int vertexCount)
{
	this->vertices.reserve(vertexCount);

	for (int i = 0; i < vertexCount; i++)
	{
		this->vertices.push_back(vertices[i]);
	}

	for (int i = 0; i < vertexCount; i++)
	{
		indices.push_back(i);
	}
}

std::vector<Vertex3> Mesh::GetVertices()
{
	return vertices;
}

std::vector<WORD> Mesh::GetIndices()
{
   	return indices;
}


AxisAlignedBox Mesh::GenerateAABB()
{
	float topX = 0, topY = 0, topZ = 0;
	float bottomX = 0, bottomY = 0, bottomZ = 0;

	for (int i = 0; i < vertices.size(); i++)
	{
		if (vertices[i].position.x > topX)
		{
			topX = vertices[i].position.x;
		}
		if (vertices[i].position.y > topY)
		{
			topY = vertices[i].position.y;
		}
		if (vertices[i].position.z > topZ)
		{
			topZ = vertices[i].position.z;
		}

		if (vertices[i].position.x < bottomX)
		{
			bottomX = vertices[i].position.x;
		}
		if (vertices[i].position.y < bottomY)
		{
			bottomY = vertices[i].position.y;
		}
		if (vertices[i].position.z < bottomZ)
		{
			bottomZ = vertices[i].position.z;
		}
	}

	DirectX::XMFLOAT3 Center((topX + bottomX) / 2.0f, (topY + bottomY) / 2.0f, (topZ + bottomZ) / 2.0f);
	DirectX::XMFLOAT3 Extents((topX - bottomX) / 2.0f, (topY - bottomY) / 2.0f, (topZ - bottomZ) / 2.0f);

	AxisAlignedBox rtn;
	rtn.Center = Center;
	rtn.Extents = Extents;

	return rtn;
}

Mesh::~Mesh()
{
}
